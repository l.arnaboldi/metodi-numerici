\documentclass[10pt,a4paper]{article}

\usepackage[italian]{babel}
\usepackage[utf8]{inputenc}
\usepackage[paper=a4paper, left=2.5cm, right=2.5cm, top=2.5cm, bottom=2.5cm]{geometry}
\usepackage[section]{placeins}
\usepackage[indent=0cm]{parskip}
\usepackage[colorlinks, linkcolor=blue, urlcolor=magenta, citecolor=teal, bookmarks]{hyperref}
\usepackage{graphicx}
\usepackage{physics}
\usepackage{bbold}
\usepackage{hyperref}
\usepackage{verbatim}
\usepackage{tikz}
\usetikzlibrary{calc}
\usepackage[backend=bibtex]{biblatex}
\addbibresource{bibliography.bib}
\usepackage{caption}
\usepackage{subcaption}

\newcommand{\paginavuota}{\newpage\null\thispagestyle{empty}\newpage}

\begin{document}

%TITOLO


\begin{titlepage}
\title{\textsc{Modello di Potts su reticolo esagonale}}
\author{Luca Arnaboldi \and Marianna Crupi}

\maketitle
\includegraphics[width=\textwidth]{img/3d.pdf}
\thispagestyle{empty} 


\end{titlepage}

%\paginavuota

%SCOPO DELL'ESPERIMENTO 

\section{Scopo del progetto}

In questo progetto studieremo il modello di Potts su reticolo esagonale in 2 dimensioni. Più nel dettaglio considereremo come possibili spin i valori $0,1, 2, 3$. L'hamiltoniana di interazione del modello considerato è data da:
\begin{equation}
	H = -J \sum_{<i,j>} \delta_{\sigma_i \sigma_j},  %- h \sum_i \sigma_i
\end{equation}
dove con $J$ abbiamo indicato la costante di accoppiamento tra spin adiacenti.
	
In questo progetto abbiamo considerato come base del reticolo di Ising la cella fondamentale del reticolo esagonale, dunque ogni cella contiene due siti del reticolo. Abbiamo poi preso gli assi $x$ e $y$ paralleli ai due lati del rombo, come mostrato in Figura \ref{reticolo} e dunque il nostro reticolo finale non sarà un quadrato, ma bensì un rombo formato da $L \times L$ celle fondamentali. Le condizioni al bordo sono periodiche.
\begin{figure}
  \centering
  \resizebox{0.75\textwidth}{!}{\input{img/cella.tikz}}
  \caption{schema del reticolo e della cella fondamentale utilizzata. Sono riportate le condizioni al bordo periodiche per un singolo spin, a titolo d'esempio.}
  \label{reticolo}
\end{figure}

Ogni sito sarà dunque identificato da tre coordinate: le coordinate della cella $x$, $y$ e la posizione all'interno della cella fondamentale $d = 0,1$. Indicheremo dunque le coordinate di un sito come $(x, y; d)$.

Nel calcolo dell'interazione a primi vicini avremo che gli spin con cui ogni sito si accoppia dipenderanno dal valore di $d$:
\begin{itemize}
	\item{$(x, y; 0)$:} il sito sarà accoppiato con i siti $(x, y; 1)$, $(x-1, y; 1)$ e $(x, y-1; 1)$;
	\item{$(x, y; 1)$:} si accoppierà con $(x, y; 0)$, $(x+1, y; 0)$ e $(x, y+1; 0)$.
\end{itemize}

Dalle prime simulazioni è emerso un comportamento singolare simile a quello mostrato dal modello di Ising 2D, ovvero con una transizione di fase del second'ordine. Ci siamo dunque posti come scopo quello di investigare tale comportamento singolare.

Si pone ora il problema di definire la \emph{magnetizzazione} in questo modello. Partendo dal presupposto che tale quantità dovrà avere le proprietà di parametro d'ordine, una possibile definizione è
\[
  M \equiv \sum_{s=0}^3 N_s e^{i\frac{\pi}2 s}
\]
dove con \(N_s\) si intende il numero di siti con spin \(s\).

In particolare lo scopo del progetto sarà quello di stimare la temperatura critica $\beta_c$ del sistema e gli esponenti critici $\alpha$, $\beta$, $\gamma$ e $\nu$ che forniscono l'andamento delle seguenti quantità in un intorno della temperatura critica:
\begin{align*}
	\xi &\sim |t|^{\nu} \\
	\langle M \rangle &\sim |t|^{\beta} \quad \text{per} \  T<T_c\\
	\chi &\propto V(\langle |M|^2 \rangle - (\langle |M| \rangle)^2) \sim |t|^{\gamma / \nu}\\
	C &\equiv \frac{\partial E}{\partial T} \propto V(\langle E^2 \rangle - (\langle E \rangle)^2) \sim |t|^{\alpha / \nu}
\end{align*}
dove con $t$ si intende la temperatura ridotta $(T-T_c)/T_c$, con $\xi$ la lunghezza di correlazione del sistema, con $M$ la magnetizzazione totale, con $\chi$ la suscettività\footnote{Bisogna prestare particolare attenzione al fatto che la magnetizzazione non si può più definire nella maniera usuale come si faceva per il modello di Ising (derivata rispetto al campo). Questione ancora più delicata è la suscettività che non è per niente chiaro come definire; noi abbiamo scelto questa definizione per mantenere un'analogia con il modello di Ising, ma questa quantità non ha nessun significato fisico, nonostante presenti comportamenti critici.}, con $V$ il numero di siti del reticolo, con $C$ il calore specifico e con $E$ l'energia.

La nostra trattazione si baserà pesantemente sull'ipotesi del \emph{finite size scaling} per la stima degli esponenti critici. Assumeremo quindi che $\xi$ può valere al massimo $L$. 

\section{Simulazioni numeriche}
Nella trattazione assumeremo \(J=k=1\).

Per poter confrontare reticoli di dimensioni differenti le quantità che saremo interessati a simulare non saranno quelle indicate sopra, ma i loro corrispondenti intensivi. Più in particolare misureremo le seguenti quantità:
\begin{equation}
	 m_r = \frac{N_0 - N_2}{2L^2} ; \qquad m_i = \frac{N_1 - N_3}{2L^2} ;  \qquad \epsilon = \frac{E}{2L^2} \equiv \frac{1}{2L^2}\left(-\sum_{\langle i,j \rangle} \delta_{\sigma_i \sigma_j} \right),
\end{equation}
dove con \(m_r\) e \(m_i\) indichiamo rispettivamente parte reale ed immaginaria della magnetizzazione. La quantità a cui saremo interessati sarà poi \(|m|\) siccome l'equiprobabilità delle fasi porterebbe al banale risultato \(\langle m\rangle = 0\). Per quanto riguarda suscetiività e calore specifico le quantità che andremo a misusrare sono:
\begin{equation}
	\chi = 2L^2 \sigma^2(|m|); \qquad
  c = 2L^2 \sigma^2(\epsilon),
\end{equation}
ovvero i corrispondenti intensivi da cui sono stati rimossi eventuali fattoti dipendenti dalla temperatura, in quanto non ne modificano il comportamento singolare al punto critico. 

\subsection{Metropolis-Hastings}
Per la simulazione del reticolo abbiamo utilizzato l'algoritmo di Metropolis-Hastings per il campionamento della distribuzione di Boltzman. \\
Più nel dettaglio come prima cosa abbiamo inizializzato il reticolo in modo da avere tutti gli spin posti a zero. Ad ogni \emph{update} abbiamo selezionato in maniera indipendente le tre coordiante $(x, y; d)$ di un sito del reticolo, con spin $s$, e scelto lo spin candidato $\tilde{s}$ nel seguente modo:
\begin{itemize}
	\item scegliamo randomicamente un valore tra 1, 2 e 3;
	\item sommiamo il valore ottenuto al valore dello spin $s$ del sito selezionato;
	\item prendiamo come spin candidato il valore ottenuto modulo 4.
\end{itemize}
In questo modo siamo sicuri di non selezionare $\tilde{s} = s$ e otteniamo una distribuzione equiprobabile tra i restanti tre valori di spin. A questo punto:
\begin{itemize}
	\item estraiamo un valore reale $q$ con distribuzione uniforme tra 0 e 1;
	\item se $q < e^{-\beta (H(\tilde{s})-H(s))}$ poniamo $s = \tilde{s}$.
\end{itemize}

Dove con $H(\sigma)$ indichiamo il valore dell'Hamiltoniana con spin $\sigma$ sul sito selezionato dall'algoritmo. Risulta evidente che per $e^{-\beta H(s)}<e^{-\beta H(\tilde{s})}$ il valore dello spin risulterà certamente mutato.

Per avere un adeguato \emph{refresh} ogni \emph{step} del Metropolis-Hasting sarà per noi costituito da \(2L^2\) \emph{updates}.

Per procedere all'analisi delle simulazioni ottenute sarà necessario individuare il numero di iterazioni dell'algoritmo necessarie a portare a termalizzazione il sistema. Bisognerà dunque individuare tale parametro che indicheremo con \texttt{N\_STARTING}. La scelta della temperatura per la calibrazione dei parametri è stata fatta tentando di avere un valore prossimo, leggermente superiore, alla temperatura critica.

\subsubsection{Calibrazione di \texttt{N\_STARTING}}

Per stimare il tempo necessario per raggiungere termalizzazione abbiamo eseguito un plot di $m$ ed $\epsilon$ ai vari step dell'algoritmo. Dal grafico riportato in Fig. \ref{fig:nstarting} risulta evidente che la fase di correlazione dura non più di 2500 step. Abbiamo dunque scelto di porre \texttt{N\_STARTING} = 10000.

\begin{center}
	\begin{figure}[h]
		\includegraphics[width = \linewidth]{img/nstarting.pdf}
		\caption{Grafico di $m$ (sinistra) ed $\epsilon$ (destra) a vari step dell'algoritmo di Metropolis 	Hastings. Il grafico è ottenuto per valori di $\beta = 1.62$ e $L = 35$.}
		\label{fig:nstarting}
	\end{figure}
\end{center}

\subsubsection{Tempo di correlazione}
Sapendo che i dati ottenuti presenteranno una forte correlazione, ci interessa stimare il tempo necessario a renderla trascurabile. Indicheremo tale tempo con $\tau$.

Per la stima di $\tau$ faremo uso della magnetizzazione. Indicando con $m(k)$ la $k$-esima misura della magnetizzazione e con $N$ il numero totale di misurazioni, possiamo definire una funzione di correlazione al tempo $j$ come:
\begin{equation}
	\tilde{C}_m(j) = \frac1{N-j} \sum_{k=0}^{N-j} m(k)m(k+j) - \left( \frac1N\sum_{k=0}^{N} m(k)\right)^2.
	\label{no}
\end{equation}

Tuttavia, seguendo quanto riportato in \cite{newman1999monte} abbiamo utilizzato, invece, la funzione di correlazione:
\begin{equation}
	C_m(j) = \frac1{N-j} \sum_{k=0}^{N-j} m(k)m(k+j) - \left( \frac1{N-j}\sum_{k=0}^{N-j} m(k)\right)\left( \frac1{N-j}\sum_{k=0}^{N-j} m(k+j)\right).
	\label{si}
\end{equation}
%che rispetto alla Eq. \eqref{no} presenta un andamento più regolare. \\
Il confronto tra le due differenti funzioni di correlazione è riportato in Figura \ref{fig:corr} e possiamo vedere che la funzione di Newmann presenta un andamento più regolare rispetto alla Eq. \eqref{no}. Dal grafico riportato è inoltre possibile stimare il tempo di correlazione
\begin{equation*}
	\tau = 2000.
\end{equation*}
Per iniziare a ridurre tale valore di circa un fattore 10, non salveremo tutte le misure, bensì una sola ogni 10 step del Metropolis-Hastings. Chiameremo tale valore \emph{tempo di decorrelazione}.

\begin{center}
	\begin{figure}
    \centering
		\includegraphics[width = 0.75\linewidth]{img/correlationfunction.pdf}
		\caption{Grafico delle funzioni di correlazione definite dalla formula standard \eqref{no} (in blu) e dalla formula di Newmann \eqref{si} (in verde).}
		\label{fig:corr}
	\end{figure}
\end{center}


\subsubsection{Blocking}
In base a quanto ottenuto nella sezione precedente, è chiaro che salvare una sola misura su 10 non elimina del tutto la correlazione tra i dati. Per risolvere definitivamente il problema e riuscire a stimare correttamente l'errore su energia e magnetizzazione abbiamo utilizzato la tecnica del \textit{blocking}.\\
Per fissare la dimensione del blocco abbiamo calcolato l'errore su energia e magnetizzazione per differenti grandezze dei blocchi, tentando di identificare una zona di stabilizzazione. Da quanto riportato in Figura \ref{fig:blocking} possiamo vedere che il valore delle incertezze inizia a stabilizarsi per una dimensione dei blocchi di $2^9=512$. \\
Abbiamo dunque selezionato tale valore come dimensione dei blocchi nel nostro algoritmo.

\begin{center}
	\begin{figure}
    \centering
		\includegraphics[width = 0.75\linewidth]{img/blocking.pdf}
		\caption{Grafico degli errori su energia e magnetizzazione ottenuto per $\beta = 1.62$, $L=35$, 100.000 misure e un tempo di decorrelazione di 10 misure.}
		\label{fig:blocking}
	\end{figure}
\end{center}


\subsubsection{Bootstrap}
Essendoci accertati di star analizzando dati decorrelati, possiamo adesso applicare la tecnica del  \textit{bootstrap} al fine di stimare l'errore su calore specifico e suscettività. Una volta fissati i parametri come detto nelle sezioni precedenti, rimane da scegliere $N_r$, il numero di resample del \textit{bootstrap}.

A tal fine plottiamo dunque il valore di $\sigma_c$ e $\sigma_{\chi}$ per crescenti valori di $N_r$ tentando di identificare la zona di stabilizzazione. Da quanto riportato in Figura \ref{fig:bootstrap} possiamo scegliere 
\begin{equation*}
	N_r = 1200.
\end{equation*}

\begin{center}
	\begin{figure}
    \centering
		\includegraphics[width = 0.9\linewidth]{img/bootstrap.pdf}
		\caption{Grafico di $\sigma_c$ e $\sigma_{\chi}$ per crescenti valori di $N_r$.}
		\label{fig:bootstrap}
	\end{figure}
\end{center}

\subsection{Simulazioni effettuate}
Precedendo come descritto nelle sezioni precedenti abbiamo dunque simulato l'andamento di $m$, $\epsilon$, $\chi$ e $c$ per differenti valori di $L$. In particolare abbiamo eseguito le suddette simulazioni per valori di $L$ da $5$ a $50$, di tre in tre. Per quanto riguarda \(\beta\), sono stati simulati 50 valori equispaziati tra 1.35 ed 1.90; inoltre per analizzare più nel dettaglio l'intorno della temperatura critica abbiamo aggiunto 100 simulazioni equispaziate tra 1.45 e 1.75. A fissati valori di \(L\) e \(\beta\), abbiamo eseguito $100'000$ simulazioni.

Abbiamo utilizzato il linguaggio \texttt{C++} per l'implementazione dell'algoritmo MH, mentre abbiamo usato \texttt{Python} per l'analisi dati. Il generatore di numeri casuali che abbiamo utilizzato è l'implementazione di un Mersenne Twister fornita dalla libreria standard del \texttt{C++}. Tutto il codice scritto per le simulazioni è disponibile in \href{https://gitlab.com/l.arnaboldi/metodi-numerici/-/tree/master/modulo1}{questa repository}.

Sono di seguito riportati i risultati delle simulazioni per $L=5, 20, 35, 50$ della magnetizzazione (Figura \ref{fig:magn}), dell'energia (Figura \ref{fig:en}), della suscettività (Figura \ref{fig:sushi}) e del calore specifico (Figura \ref{fig:sh}).

\begin{figure}
	\centering
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\linewidth]{img/5-magnetization.pdf}
	\end{subfigure}\hfil
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\linewidth]{img/20-magnetization.pdf}
	\end{subfigure}
	
	\medskip
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\linewidth]{img/35-magnetization.pdf}
	\end{subfigure}\hfil
	\begin{subfigure}{0.4\textwidth}
	\includegraphics[width=\linewidth]{img/50-magnetization.pdf}
	\end{subfigure}

	\caption{Grafico della magnetizzazione per $L=5, 20, 35, 50$ in ordine calligrafico.}
	\label{fig:magn}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\linewidth]{img/5-energy.pdf}
	\end{subfigure}\hfil
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\linewidth]{img/20-energy.pdf}
	\end{subfigure}
	
	\medskip
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\linewidth]{img/35-energy.pdf}
	\end{subfigure}\hfil
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\linewidth]{img/50-energy.pdf}
	\end{subfigure}
	
	\caption{Grafico dell'energia per $L=5, 20, 35, 50$ in ordine calligrafico.}
	\label{fig:en}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\linewidth]{img/5-sushi.pdf}
	\end{subfigure}\hfil
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\linewidth]{img/20-sushi.pdf}
	\end{subfigure}
	
	\medskip
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\linewidth]{img/35-sushi.pdf}
	\end{subfigure}\hfil
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\linewidth]{img/50-sushi.pdf}
	\end{subfigure}
	
	\caption{Grafico della suscettività per $L=5, 20, 35, 50$ in ordine calligrafico.}
	\label{fig:sushi}
\end{figure}

\begin{figure}
	\centering
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\linewidth]{img/5-specific-heat.pdf}
	\end{subfigure}\hfil
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\linewidth]{img/20-specific-heat.pdf}
	\end{subfigure}
	
	\medskip
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\linewidth]{img/35-specific-heat.pdf}
	\end{subfigure}\hfil
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\linewidth]{img/50-specific-heat.pdf}
	\end{subfigure}
	
	\caption{Grafico del calore specifico per $L=5, 20, 35, 50$ in ordine calligrafico.}
	\label{fig:sh}
\end{figure}





\section{Analisi dei risultati}
Una volta ottenute le misurazioni delle quantità termodinamiche, abbiamo proceduto con il calcolo degli esponenti critici del sistema. Per eseguire i \emph{fit} abbiamo utilizzato la funzione \texttt{curve\_fit} del paccheto \texttt{scipy}, mentre per la propagazione degli errori ci siamo serviti del pacchetto \texttt{uncertainties}.

\subsection{Stima di $\nu$}

Fondandoci sull'ipotesi di \textit{scaling}, si ottiene un andamento per la magnetizzazione $\chi$ del tipo:
\begin{equation}
	\chi ( \beta, L ) = L^{-\gamma / \nu} \hat{\phi} (L, \xi ).
	\label{chi}
\end{equation}
Assumendo adesso, in analogia al modello di Ising, che il sistema perda memoria della struttura microscopica nei pressi della transzione, abbiamo che l'unica quantità rilevante per il nostro sistema sarà $L / \xi$. Sotto questa ipotesi possiamo dunque riscrivere \eqref{chi} come:
\begin{equation}
	\chi (\beta, L) = L^{\gamma / \nu } \phi \left( \left(\beta- \beta_c\right) L^{1/\nu}\right).
	\label{max}
\end{equation}
Detto quindi $\beta_{pc}(L)$ il punto in cui si realizza il massimo in \(\beta\) di $\chi(\beta, L )$ e $\bar{x}$ il punto che massimizza $\phi$, dalla Eq. \eqref{max} si trova facilmente la relazione:
\begin{equation}
	\beta_{pc}(L) = \beta_c + \bar{x} L^{-1/\nu}.
	\label{betapc}
\end{equation}
Abbiamo dunque stimato $\beta_{pc}$ a diversi $L$ come il punto che realizza il valore di suscettività più alto nella nostra simulazione, mentre l'errore è stato assegnato come la spaziatura tra i valori di $\beta$ per le diverse simulazioni.\\
Abbiamo dunque eseguito un \textit{fit} per la funzione Eq. \eqref{betapc} lasciando come parametri liberi $\beta_c$, $\bar{x}$ e $\nu$, ottenendo i valori:
\begin{equation}
	\beta_c = 1.6085 \pm 0.0013, \qquad \nu = 0.688 \pm 0.028, \qquad \bar{x} = 1.33 \pm 0.13.
	\label{nu}
\end{equation}  
I risultati del \textit{fit} sono riportati in Figura \ref{fig:nu}.

\begin{center}
	\begin{figure}[h]
		\includegraphics[width = \linewidth]{img/nu-betac.pdf}
		\caption{Grafico del \textit{fit} dell'Eq. \eqref{betapc} con relativi residui rinormalizzati.}
		\label{fig:nu}
	\end{figure}
\end{center}

\subsection{Stima di $\gamma$}

La stima di $\gamma$ si fonda sull'ipotesi di \textit{finite size scaling}, che si ha quando la lunghezza tipica del sistema $\xi$ è minore o confrontabile con $L$. In tale regime possiamo immaginare che il massimo valore della suscettività magnetica sia raggiunto per valori proporzionali al massimo $\xi$ posssibile, elevato alla $\gamma/\nu$. Essendo però il massimo $\xi$ coincidente con $L$ a causa della dimensione finita del reticolo, si avrà:
\begin{equation}
	\chi_{max} \propto L^{\gamma / \nu}.
	\label{chimax}
\end{equation}
Da qui è chiaramente possibile stimare $\gamma/ \nu$ tramite \textit{fit}. In particolare noi abbiamo stimato $\chi_{max}$ come il massimo valore di $\chi$ ottenuto con le simulazioni e come errore quello dato dal \emph{bootstrap}. Abbiamo dunque eseguito il \textit{fit}  della funzione:
\begin{equation}
	\log\chi_{max} = \frac{\gamma}{\nu} \log L + c_{\gamma}
	\label{chifit}
\end{equation}
lasciando come parametro libero $\gamma / \nu$ e $c_{\gamma}$. Abbiamo ottenuto:
\begin{equation*}
	\frac{\gamma}{\nu} = 1.831 \pm 0.004 \qquad c_{\gamma} = -1.799 \pm 0.006
\end{equation*}
Da cui, utilizzando Eq. \eqref{nu} otteniamo:
\begin{equation}
	\gamma = 1.26 \pm 0.05
	\label{gamma}
\end{equation}
I risultati del \textit{fit} sono riportati in Figura \ref{fig:gamma}.

\begin{center}
	\begin{figure}[h]
		\includegraphics[width = \linewidth]{img/gamma.pdf}
		\caption{Grafico del \textit{fit} dell'Eq. \eqref{chifit} con relativi residui rinormalizzati.}
		\label{fig:gamma}
	\end{figure}
\end{center}


\subsection{Stima di $\alpha$}

In maniera del tutto analoga a quanto discusso in merito alla suscettività, per quanto riguarda il calore specifico si ha:
\begin{equation}
	c_{max} \propto L^{\alpha / \nu}.
	\label{cmax}
\end{equation}
Dunque, eseguendo il \textit{fit} della funzione 
\begin{equation}
	\log c_{max} = \frac{\alpha}{\nu} \log L + c_{\alpha}
	\label{cfit}
\end{equation}
lasciando $\alpha/\nu$ e $c_{\alpha}$ come parametri liberi, abbiamo ottenuto:
\begin{equation*}
	\frac{\alpha}{\nu} = 1.341 \pm 0.004 \qquad c_{\alpha} = 0.058 \pm 0.008.
\end{equation*}
Da cui, utilizzando il valore ottenuto precedente per $\nu$ otteniamo per $\alpha$ il valore:
\begin{equation}
	\alpha = 0.92 \pm 0.04
	\label{alpha}
\end{equation}
I risultati del \textit{fit} sono riportati in Figura \ref{fig:alpha}.

\begin{center}
	\begin{figure}[h]
		\includegraphics[width = \linewidth]{img/alpha.pdf}
		\caption{Grafico del \textit{fit} dell'Eq. \eqref{cfit} con relativi residui rinormalizzati.}
		\label{fig:alpha}
	\end{figure}
\end{center}

\subsection{Stima di $\beta$}

Dalle equazioni discusse nella prima sezione, per un intorno sinistro della temperatura critica (destro per \(\beta\)), vale:
\begin{equation}
	\langle|m|\rangle  \sim \xi^{-\beta / \nu}.
\end{equation}
Dunque, per la stima di $\beta$ abbiamo considerato il valore di $\langle|m|\rangle$ corrispondente al primo \(\beta\) superiore al valore critico (ovvero $\beta^+ = 1.6106$) al variare di $L$. Indicheremo tale valore con $\langle|m^+|\rangle$. Ci aspettiamo quindi un andamento del tipo:
\begin{equation}
	\log \langle|m^+|\rangle  = -\frac{\beta}{\nu} \log L + c_{\beta}.
	\label{mfit}
\end{equation}
Eseguendo un \textit{fit} con parametro libero $-\beta / \nu$ e $c_{\beta}$ abbiamo ottenuto i valori:
\begin{equation*}
	-\beta / \nu = -0.1159 \pm 0.0028 \qquad c_{\beta} = -0.006 \pm 0.006
\end{equation*}
Da cui, utilizzando il valore ottenuto precedente per $\nu$ otteniamo per $\beta$ il valore:
\begin{equation}
	\beta = 0.080 \pm 0.004
	\label{beta}
\end{equation}
I risultati del \textit{fit} sono riportati in Figura \ref{fig:beta}.

\begin{center}
	\begin{figure}[h]
		\includegraphics[width = \linewidth]{img/beta.pdf}
		\caption{Grafico del \textit{fit} dell'Eq. \eqref{mfit} con relativi residui rinormalizzati.}
		\label{fig:beta}
	\end{figure}
\end{center}


\section{Conclusioni}

Dall'analisi effettuata risulta, come atteso, che il sistema presenta una transizione del secondo ordine alla temperatura critica $\beta_{pc} (L)$. I potenziali termodinamici, infatti, non presentano un andamento che possa far pensare a una discontinuità nel limite $L \rightarrow +\infty$.\\
Per effettuare una verifica dei risultati ottenuti nelle sezioni precedenti, abbiamo eseguito il \textit{plot} delle quantità riscalate secondo l'ipotesi di \textit{finite size scaling}, aspettandoci di trovare un andamento alla temperatura critica indipendente dalla dimensione del reticolo. I risultati riportati in Figura \ref{fig:fss} rispecchiano qualitativamente l'andamento aspettato.

\begin{figure}
	\centering
	\vspace{-50pt}
	\begin{subfigure}{0.6\textwidth}
		\includegraphics[width=\linewidth]{img/globalsushi.pdf}
	\end{subfigure}\hfil
	\begin{subfigure}{0.6\textwidth}
		\includegraphics[width=\linewidth]{img/globalsh.pdf}
	\end{subfigure}

	\medskip
	\begin{subfigure}{0.6\textwidth}
		\includegraphics[width=\linewidth]{img/globalmagn.pdf}
	\end{subfigure}
\caption{Grafico delle quantità $\chi / L^{\gamma / \nu}$ , $c / L^{\alpha / \nu}$ e  $\langle |M| \rangle / L^{-\beta / \nu}$ (rispettivamente) al varire di $L$.}
\label{fig:fss}
\end{figure}





\clearpage
\printbibliography

\end{document}
