
import sys
import matplotlib.pyplot as plt 
import numpy as np

from simulations_parameters import Ls

critical_beta = 1.6085
nu = 0.688
alpha =  0.92
gamma = 1.26
ce_beta = 0.080

plt.style.use('seaborn')
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble='\\usepackage{amsmath} \\usepackage{siunitx}')


def main():
  directory = sys.argv[1]
  output_directory = sys.argv[2]
  fig_energy, ax_energy = plt.subplots(figsize=(7,6))
  fig_magnetization, ax_magnetization = plt.subplots(figsize=(7,6))
  fig_specific_heats, ax_specific_heats = plt.subplots(figsize=(7,6))
  fig_sushi, ax_sushi = plt.subplots(figsize=(7,6))

  for L in Ls:
    beta, energies, sigma_energies, magnetizations, sigma_magnetizations, specific_heats, sigma_specific_heats, sushi, sigma_sushi = np.loadtxt(directory+f'/{L}-mcmc-extracted-data.txt', unpack=True)
    ax_sushi.plot((beta-critical_beta)*L**(1./nu),sushi/L**(gamma/nu), linestyle='', marker='o', markersize=3, label=f'${L}$')
    ax_specific_heats.plot((beta-critical_beta)*L**(1./nu),specific_heats/L**(alpha/nu), linestyle='', marker='o', markersize=3, label=f'${L}$')
    ax_magnetization.plot((beta-critical_beta)*L**(1./nu), magnetizations/L**(-ce_beta/nu), linestyle='', marker='o', markersize=3, label=f'${L}$')

  ax_sushi.set_xlim(-15,10)
  ax_specific_heats.set_xlim(-15,10)
  ax_magnetization.set_xlim(-15,10)

  ax_specific_heats.set_ylim(0.3,1.15)

  ax_sushi.legend()
  ax_specific_heats.legend()
  ax_magnetization.legend()

  ax_sushi.set_xlabel('$\\left(\\beta-\\beta_c\\right)L^{1/\\nu}$')
  ax_specific_heats.set_xlabel('$\\left(\\beta-\\beta_c\\right)L^{1/\\nu}$')
  ax_magnetization.set_xlabel('$\\left(\\beta-\\beta_c\\right)L^{1/\\nu}$')

  ax_sushi.set_ylabel('$\\chi/L^{\\gamma/\\nu}$')
  ax_specific_heats.set_ylabel('$c/L^{\\alpha/\\nu}$')
  ax_magnetization.set_ylabel('$|m|/L^{-\\beta/\\nu}$')

  fig_specific_heats.savefig(f'{output_directory}/globalsh.pdf', format = 'pdf', bbox_inches = 'tight')
  fig_sushi.savefig(f'{output_directory}/globalsushi.pdf', format = 'pdf', bbox_inches = 'tight')
  fig_magnetization.savefig(f'{output_directory}/globalmagn.pdf', format = 'pdf', bbox_inches = 'tight')

  

if __name__ == '__main__':
  main()