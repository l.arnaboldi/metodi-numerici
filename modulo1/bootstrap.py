
import numpy as np

def bootstrap_std(estimator, data, n_resample):
  return np.std(np.array(
    [estimator(data[np.random.randint(0,len(data),len(data))])
    for r in range(n_resample)
  ]))

##############################################################################################

def test_estimator_(data):
  return np.mean(data**4)/(3*np.mean(data**2))

def main():
  s = np.random.normal(0, 1., 1000000)
  print(test_estimator_(s), bootstrap_variance(test_estimator_, s, 300))

if __name__ == "__main__":
    main()
