
import numpy as np
import sys
from tqdm import tqdm

from simulations_parameters import Ls, betas
from bootstrap import bootstrap_std
from blocking import blocking, blocking_std

simulations = {L:betas for L in Ls}
N_RESAMPLE = 1200
BLOCKING_SIZE = 9

def main():
  directory = sys.argv[1]

  for L, beta_list in simulations.items():
    beta_array = np.array(beta_list)
    energies = []
    sigma_energies = []
    magnetizations = []
    sigma_magnetizations = []
    specific_heats = []
    sigma_specific_heats = []
    sushi = []
    sigma_sushi = []
    for beta in tqdm(beta_list):
      try:
        m_r, m_c, e = np.loadtxt(directory+f'/{L}-{beta}/measures.txt', unpack=True)
      except:
        print(L, beta)
        exit(1)
      m = np.vectorize(complex)(m_r, m_c)

      energies.append(np.mean(e))
      sigma_energies.append(blocking_std(e, BLOCKING_SIZE))

      magnetizations.append(np.mean(abs(m)))
      sigma_magnetizations.append(blocking_std(abs(m), BLOCKING_SIZE))

      be = blocking(e, BLOCKING_SIZE)
      specific_heats.append((2*(L)**2)*np.std(e))
      sigma_specific_heats.append(bootstrap_std(lambda x: (2*(L)**2)*np.var(x),
                                                     be,
                                                     N_RESAMPLE))

      bs = blocking(abs(m), BLOCKING_SIZE)
      sushi.append((2*(L)**2)*np.var(abs(m)))
      sigma_sushi.append(bootstrap_std(lambda x: (2*(L)**2)*np.var(x),
                                            bs,
                                            N_RESAMPLE))
    print(f'Computed L = {L}')
    energies = np.array(energies)
    sigma_energies = np.array(sigma_energies)
    magnetizations = np.array(magnetizations)
    sigma_magnetizations = np.array(sigma_magnetizations)
    specific_heats = np.array(specific_heats)
    sigma_specific_heats = np.array(sigma_specific_heats)
    sushi = np.array(sushi)
    sigma_sushi = np.array(sigma_sushi)

    np.savetxt(directory+f'/{L}-mcmc-extracted-data.txt', np.c_[
                                                            beta_array,
                                                            energies,
                                                            sigma_energies,
                                                            magnetizations,
                                                            sigma_magnetizations,
                                                            specific_heats,
                                                            sigma_specific_heats,
                                                            sushi,
                                                            sigma_sushi
                                                          ])

    

if __name__ == "__main__":
  main()