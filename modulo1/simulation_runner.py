from pathlib import Path
import sys
import subprocess
from multiprocessing.pool import ThreadPool

from simulations_parameters import betas, Ls

SEED = 64770
N_STARTING = 10000
N_MEASURE = 100000
N_DECORRELATION = 10
EXTERNAL_FIELD = 0.

N_PROCESS = 24

def run_simulation(potts_command, directory, L, beta):
  subprocess.run([potts_command, directory+f'/{L}-{beta}/configuration-file.txt', directory+f'/{L}-{beta}/'],
                 stdout=subprocess.DEVNULL,
                 stderr=subprocess.DEVNULL)
  print(f'Computed L={L}, beta={beta}')

def main():
  directory = sys.argv[1]
  potts_command = sys.argv[2]

  tp = ThreadPool(N_PROCESS)

  for L in Ls:
    for beta in betas:
      Path(directory+f'/{L}-{beta}').mkdir(parents=True, exist_ok=True)
      with open(directory+f'/{L}-{beta}/configuration-file.txt', 'w') as filehandle:
        filehandle.write(f'{L}\n{int(SEED+L*(1e4*beta))}\n{N_MEASURE}\n{N_STARTING}\n{N_DECORRELATION}\n{beta}\n{EXTERNAL_FIELD}\n')
      tp.apply_async(run_simulation, (potts_command, directory, L, beta))
  tp.close()
  tp.join()
  





if __name__ == "__main__":
  main()