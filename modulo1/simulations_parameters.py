import numpy as np 

betas = np.concatenate([np.linspace(1.35, 1.9, 50), np.linspace(1.45, 1.75, 100)])
Ls = [5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35, 38, 41, 44, 47, 50]