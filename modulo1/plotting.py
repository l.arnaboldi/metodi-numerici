
import sys
import matplotlib.pyplot as plt 
import numpy as np
from tqdm import tqdm
from pathlib import Path
from matplotlib import cm

from simulations_parameters import Ls

plt.style.use('seaborn')
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble='\\usepackage{amsmath} \\usepackage{siunitx}')

def savefig(x, y, uy, figname, xlabel = "", ylabel = "", title = "", directory = '.'):
  fig, ax = plt.subplots(figsize=(7,6))
  ax.errorbar(x, y, yerr=uy, fmt='o', markersize=2, capsize=1, linestyle='', elinewidth=0.4)
  ax.set_xlabel(xlabel)
  ax.set_ylabel(ylabel)
  ax.set_title(title)
  fig.savefig(f'{directory}/plots/{figname}.pdf', format = 'pdf', bbox_inches = 'tight')

def main():
  directory = sys.argv[1]
  Path(directory+'/plots/').mkdir(parents=True, exist_ok=True)
  
  L3 = []
  beta3 = []
  sushi3 = []
  magn3 = []
  cv3 = []
  for L in tqdm(Ls):
    beta, energies, sigma_energies, magnetizations, sigma_magnetizations, specific_heats, sigma_specific_heats, sushi, sigma_sushi = np.loadtxt(directory+f'/{L}-mcmc-extracted-data.txt', unpack=True)
    # savefig(beta, energies, sigma_energies, f'{L}-energy', r'$\beta$', r'$\epsilon$', directory=directory)
    # savefig(beta, magnetizations, sigma_magnetizations, f'{L}-magnetization', r'$\beta$', r'$m$', directory=directory)
    # savefig(beta, specific_heats, sigma_specific_heats, f'{L}-specific-heat', r'$\beta$', r'$c$', directory=directory)
    # savefig(beta, sushi, sigma_sushi, f'{L}-sushi', r'$\beta$', r'$\chi$', directory=directory)
    beta3.extend(list(beta))
    L3.extend([L]*len(beta))
    sushi3.extend(list(sushi))
    magn3.extend(list(magnetizations))
    cv3.extend(list(specific_heats))

  plt.style.use('classic')
  fig3 = plt.figure(figsize=(7,7), facecolor='white')
  ax3 = fig3.gca(projection='3d')
  ax3.plot_trisurf(np.array(beta3), np.array(L3), np.array(cv3), cmap = cm.jet, edgecolor='none')
  ax3.view_init(elev=30., azim=120)
  ax3.axis('off')
  fig3.savefig(f'{directory}/plots/3d.pdf', format = 'pdf', bbox_inches = 'tight')

if __name__ == '__main__':
  main()