
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <random>
#include <string>
#include <utility>

using namespace std;

using real = double;

// We asssume J=1.
const unsigned int q = 4;

void metropolis_hasting(vector<vector<vector<unsigned int>>>& field, unsigned int n_cell, real& beta, mt19937& rng) {
  uniform_int_distribution<unsigned int> cell_coordinates_distribution(0,n_cell-1);
  uniform_int_distribution<unsigned int> unitary_cell_distribution(0,1);
  uniform_int_distribution<int> spin_value_distribution(1, q-1);
  uniform_real_distribution<real> probability_distribution(real(0.),real(1.));

  for (unsigned int t = 0; t < 2*n_cell*n_cell; t++) {
    unsigned int i = cell_coordinates_distribution(rng);
    unsigned int j = cell_coordinates_distribution(rng);
    unsigned int d = unitary_cell_distribution(rng);
    unsigned int actual_spin = field[i][j][d];
    unsigned int candidate_spin = (actual_spin + spin_value_distribution(rng)) % q;
    unsigned int force_actual = 0, force_candidate = 0;
    if (d == 0) {
      force_actual += (unsigned int)(field[i][j][1]==actual_spin) + 
                      (unsigned int)(field[(i+n_cell-1)%n_cell][j][1]==actual_spin) + 
                      (unsigned int)(field[i][(j+n_cell-1)%n_cell][1]==actual_spin);
      force_candidate += (unsigned int)(field[i][j][1]==candidate_spin) + 
                         (unsigned int)(field[(i+n_cell-1)%n_cell][j][1]==candidate_spin) + 
                         (unsigned int)(field[i][(j+n_cell-1)%n_cell][1]==candidate_spin);
    } else {
      force_actual += (unsigned int)(field[i][j][0]==actual_spin) + 
                      (unsigned int)(field[(i+n_cell+1)%n_cell][j][0]==actual_spin) + 
                      (unsigned int)(field[i][(j+n_cell+1)%n_cell][0]==actual_spin);
      force_candidate += (unsigned int)(field[i][j][0]==candidate_spin) + 
                         (unsigned int)(field[(i+n_cell+1)%n_cell][j][0]==candidate_spin) + 
                         (unsigned int)(field[i][(j+n_cell+1)%n_cell][0]==candidate_spin);
    }
    real transition_probability = exp((real(force_candidate)-real(force_actual))*beta);
    if (probability_distribution(rng) <= transition_probability) {
      field[i][j][d] = candidate_spin;
    }
  }
}

pair<real,real> magnetization(vector<vector<vector<unsigned int>>>& field, unsigned int n_cell) {
  int m_r = 0, m_c = 0;
  for (unsigned i = 0; i < n_cell; i++) {
    for (unsigned j = 0; j < n_cell; j++) {
      m_r += (field[i][j][0]==0) - (field[i][j][0]==2) + (field[i][j][1]==0) - (field[i][j][0]==2);
      m_c += (field[i][j][0]==1) - (field[i][j][0]==3) + (field[i][j][1]==1) - (field[i][j][0]==3);
    }
  }
  return make_pair(real(m_r)/real(2*n_cell*n_cell), real(m_c)/real(2*n_cell*n_cell));
}

real energy(vector<vector<vector<unsigned int>>>& field, unsigned int n_cell) {
  unsigned int e = 0;
  for (unsigned i = 0; i < n_cell; i++) {
    for (unsigned j = 0; j < n_cell; j++) {
      e += (unsigned int)(field[i][j][0] == field[i][j][1]) + 
           (unsigned int)(field[i][j][1] == field[(i+1)%n_cell][j][0]) + 
           (unsigned int)(field[i][j][1] == field[i][(j+1)%n_cell][0]);
    }
  }
  return -real(e)/real(2*n_cell*n_cell);
}

void save_field(vector<vector<vector<unsigned int>>>& field, unsigned int n_cell, string filename) {
  ofstream field_out(filename);
  field_out << fixed << setprecision(0);
  for (unsigned i = 0; i < n_cell; i++) {
    for (unsigned j = 0; j < n_cell; j++) {
      field_out << field[i][j][0] << ' ' << field[i][j][1] << "  ";
    }
      field_out << endl;
  }
  field_out.close();
}

void log_spins(vector<vector<vector<unsigned int>>>& field, unsigned int n_cell) {
  unsigned blue = 0, red = 0, green = 0, purple = 0;
  for (unsigned i = 0; i < n_cell; i++) {
    for (unsigned j = 0; j < n_cell; j++) {
      for (unsigned k = 0; k < 2; k++) {
        if (field[i][j][k] == 0) {
          blue++;
        } else if (field[i][j][k] == 1) {
          red++;
        } else if (field[i][j][k] == 2) {
          green++;
        } else if (field[i][j][k] == 3) {
          purple++;
        }
      }
    }
  }
  clog << "Spin blue:   " << blue << endl;
  clog << "Spin red:    " << red << endl;
  clog << "Spin green:  " << green << endl;
  clog << "Spin purple: " << purple << endl;
}

int main(int argc, char *argv[]) {
  string configuration_file(argv[1]);
  string output_folder(argv[2]);

  ifstream configuration_input(configuration_file);
  unsigned int seed, n_cell, n_measures, n_decorrelation, n_starting;
  real beta, external_field;
  configuration_input >> n_cell >> seed >> n_measures >> n_starting >> n_decorrelation >> beta;
  clog << "Loading configuration..." << endl;
  clog << "  n_cell = " << n_cell << endl;
  clog << "  seed = " << seed << endl;
  clog << "  n_measures = " << n_measures << endl;
  clog << "  n_starting = " << n_starting << endl;
  clog << "  n_decorrelation = " << n_decorrelation << endl;
  clog << "  beta = " << beta << endl;
  configuration_input.close();

  vector<vector<vector<unsigned int>>> field(n_cell, vector<vector<unsigned int>>(n_cell, vector<unsigned int>(2, 0)));
  mt19937 rng(seed);

  for (unsigned k = 0; k < n_starting; k++) {
    metropolis_hasting(field, n_cell, beta, rng);
  }

  vector<pair<real,real>> m;
  vector<real> e;
  save_field(field, n_cell, output_folder+"initial_field.out");
  clog << "|                                                                                                    |\n ";
  for (unsigned t = 0; t < n_measures; t++) {
    if ((t+1)%(n_measures/100)==0) {
      clog << '*';
    }
    for (unsigned k = 0; k < n_decorrelation; k++) {
      metropolis_hasting(field, n_cell, beta, rng);
    }
    //save_field(field, n_cell, "field_number_"+to_string(t+1)+".out");
    //log_spins(field, n_cell);
    m.push_back(magnetization(field, n_cell));
    e.push_back(energy(field, n_cell));
  }
  clog << endl;
  save_field(field, n_cell, output_folder+"final_field.out");

  ofstream result_out(output_folder+"measures.txt");
  result_out << fixed << setprecision(7);
  for (unsigned t = 0; t < n_measures; t++) {
    result_out << m[t].first << ' ' << m[t].second << ' ' << e[t] << endl;
  }
  result_out.close();

  return 0;
}
