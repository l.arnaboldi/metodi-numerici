
import numpy as np
from uncertainties import unumpy as unp
from uncertainties import ufloat
import sys
from scipy.optimize import curve_fit
import plotting 
from LabTools.fit import *
from LabTools.plot import *
import matplotlib.pyplot as plt

from simulations_parameters import Ls, betas

def line(x, m, q):
  return m*x+q

def power_line(x, m, q, ex):
  return m*unp.pow(x, -1./ex) + q

def extract_values(L, directory):
  beta, energies, sigma_energies, magnetizations, sigma_magnetizations, specific_heats, sigma_specific_heats, sushi, sigma_sushi = np.loadtxt(directory+f'/{L}-mcmc-extracted-data.txt', unpack=True)
  mi = np.argmax(sushi)
  beta_c = ufloat(beta[mi], abs(beta[mi+1] - beta[mi]))
  sushi_max = ufloat(sushi[mi], sigma_sushi[mi])
  mish = np.argmax(specific_heats)
  s_heat_max = ufloat(specific_heats[mish], sigma_specific_heats[mish])

  ib = 60
  while beta[ib] < 1.608:
    ib += 1
  print(beta[ib])
  cm = ufloat(magnetizations[ib], sigma_magnetizations[ib])
  return beta_c, sushi_max, s_heat_max, cm

def main():
  directory = sys.argv[1]
  output_directory = sys.argv[2]
  plt.style.use('classic')
  interesting_values = [extract_values(L, directory) for L in Ls]
  beta_c, sushi_max, s_heat_max, cm = map(lambda x: np.array(list(x)), zip(*interesting_values))
  L = unp.uarray(Ls, 0.)

  xbar, crititical_beta, nu = ucurve_fit(power_line, L, beta_c)
  gamma_div_nu, qg = ucurve_fit(line, unp.log(L), unp.log(sushi_max))
  alpha_div_nu, qa = ucurve_fit(line, unp.log(L), unp.log(s_heat_max))
  minus_beta_div_nu, qb = ucurve_fit(line, unp.log(L), unp.log(cm))

  alpha = alpha_div_nu*nu
  gamma = gamma_div_nu*nu
  beta = -minus_beta_div_nu*nu

  print(f'Critical beta: {crititical_beta}')
  print(f'nu: {nu}')
  print(f'alpha: {alpha}')
  print(f'gamma: {gamma}')
  print(f'beta: {beta}')
  print()
  print(f'xbar: {xbar}')
  print(f'qg: {qg}')
  print(f'qa: {qa}')
  print(f'qb: {qb}')
  print(f'gamma_div_nu: {gamma_div_nu}')
  print(f'alpha_div_nu: {alpha_div_nu}')
  print(f'minus_beta_div_nu: {minus_beta_div_nu}')

  residual_plot(
    f = power_line,
    param = (xbar, crititical_beta, nu),
    X = L,
    Y = beta_c,
    use_ux = False,
    figfile = output_directory+'/nu-betac.pdf',
    xlabel = '$L$',
    ylabel = '$\\beta_{pc}$',
  )

  residual_plot(
    f = line,
    param = (gamma_div_nu, qg),
    X = unp.log(L),
    Y = unp.log(sushi_max),
    use_ux = False,
    figfile = output_directory+'/gamma.pdf',
    xlabel = '$\\log{L}$',
    ylabel = '$\\log{\\chi_{max}}$',
  )

  residual_plot(
    f = line,
    param = (alpha_div_nu, qa),
    X = unp.log(L),
    Y = unp.log(s_heat_max),
    use_ux = False,
    figfile = output_directory+'/alpha.pdf',
    xlabel = '$\\log{L}$',
    ylabel = '$\\log{c_{max}}$',
  )

  residual_plot(
    f = line,
    param = (minus_beta_div_nu, qb),
    X = unp.log(L),
    Y = unp.log(cm),
    use_ux = False,
    figfile = output_directory+'/beta.pdf',
    xlabel = '$\\log{L}$',
    ylabel = '$\\log{|m|}$',
  )





if __name__ == "__main__":
  main()
