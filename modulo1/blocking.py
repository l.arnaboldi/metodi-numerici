
import numpy as np

def blocking_step(data):
  blocked_data = []
  for i in range(int((len(data)/2))):
    blocked_data.append((data[2*i]+data[2*i+1])/2.)
  return np.array(blocked_data)

def blocking(data, k):
  for i in range(k):
    data = blocking_step(data)
  return data


def blocking_std(data, k):
  data = blocking(data, k)
  return np.std(data)/np.sqrt(len(data)-1)

##############################################################################################

MU = 5.
STD = 1.
N = 1000000

def metropolis_hasting_normal(last, delta):
  candidate = np.random.uniform(last-delta, last+delta)
  quota = np.exp((-(candidate-MU)**2+(last-MU)**2)/(2*STD))
  if np.random.uniform(0., 1.) <= quota:
    return candidate
  else:
    return last

def main():
  x = 0.
  for i in range(10000):
    x = metropolis_hasting_normal(x, 0.1)

  s = []
  for i in range(N):
    x = metropolis_hasting_normal(x, 0.1)
    s.append(x)
  s = np.array(s)
  print(s)
  print(np.mean(s), np.std(s)/np.sqrt(len(s)-1))
  for i in range(int(np.log2(N))):
    print(blocking_std(s, i))

if __name__ == "__main__":
    main()
