from pathlib import Path
import sys
import matplotlib.pyplot as plt 
import numpy as np
import subprocess
from tqdm import tqdm
from joblib import Parallel, delayed
import multiprocessing
from blocking import blocking_std, blocking
from bootstrap import bootstrap_std

BETA_C = 1.62
L = 35
SEED = 647700

plt.style.use('seaborn')
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble='\\usepackage{amsmath} \\usepackage{siunitx}')

def n_strating(directory, output_directory, potts_command):
  N_STARTING = 0
  N_MEASURE = 10000
  N_DECORRELATION = 1
  Path(directory+'/n_starting-run').mkdir(parents=True, exist_ok=True)
  with open(directory+'/n_starting-run/conf-n_starting.txt', 'w') as filehandle:
    filehandle.write(f'{L}\n{SEED+22}\n{N_MEASURE}\n{N_STARTING}\n{N_DECORRELATION}\n{BETA_C}\n')
  subprocess.run([potts_command, directory+'/n_starting-run/conf-n_starting.txt', directory+'/n_starting-run/'])

  m_r, m_c, e = np.loadtxt(directory+f'/n_starting-run/measures.txt', unpack=True)

  m = abs(m_r+ 1j * m_c)

  fig, ax = plt.subplots(nrows=1, ncols=2,figsize=(10,5))
  axm = ax[0]
  axe = ax[1]

  axm.plot(np.array(range(N_MEASURE)), m, lw=0.3)
  axm.set_xlabel('MH steps')
  axm.set_ylabel('$m$')
  axm.set_xlim(-100,N_MEASURE)

  axe.plot(np.array(range(1,N_MEASURE)), e[1:], lw=0.5)
  axe.set_xlabel('MH steps')
  axe.set_ylabel('$\\epsilon$')
  axe.set_xlim(-100,N_MEASURE)

  fig.savefig(f'{output_directory}/nstarting.pdf', format = 'pdf', bbox_inches = 'tight')
  
def correlation_function(j,data):
  mkj = 0.
  for k in range(len(data)-j):
    mkj += data[k] * data[k+j]
  mkj /= float(len(data)-j)
  return mkj - (np.mean(data))**2

def newman_correlation_function(j, data):
  mk = 0.
  mkj = 0.
  corrm = 0.
  for k in range(len(data)-j):
    corrm += data[k] * data[k+j]
    mk += data[k]
    mkj += data[k+j]
  corrm /= float(len(data)-j)
  mk /= float(len(data)-j)
  mkj /= float(len(data)-j)
  return corrm - mk*mkj
   
def correlations_plot(directory, output_directory, potts_command):
  N_STARTING = 10000
  N_MEASURE = 100000
  N_CORR = 4000
  N_DECORRELATION = 1
  Path(directory+'/correlation-function-run').mkdir(parents=True, exist_ok=True)
  with open(directory+'/correlation-function-run/conf-n_starting.txt', 'w') as filehandle:
    filehandle.write(f'{L}\n{SEED+22}\n{N_MEASURE}\n{N_STARTING}\n{N_DECORRELATION}\n{BETA_C}\n')
  subprocess.run([potts_command, directory+'/correlation-function-run/conf-n_starting.txt', directory+'/correlation-function-run/'])

  m_r, m_c, e = np.loadtxt(directory+f'/correlation-function-run/measures.txt', unpack=True)
  m = abs(m_r+ 1j * m_c)

  nCm = np.array(Parallel(n_jobs=multiprocessing.cpu_count())(delayed(newman_correlation_function)(j, m) for j in tqdm(range(N_CORR))))
  Cm = np.array(Parallel(n_jobs=multiprocessing.cpu_count())(delayed(correlation_function)(j, m) for j in tqdm(range(N_CORR))))

  fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(10/1.5,7/1.5))

  ax.plot(np.array(range(N_CORR)), Cm/Cm[0], lw=0.8, label = 'Standard Correlation')
  ax.plot(np.array(range(N_CORR)), nCm/nCm[0], lw=0.8, label = 'Newman Formula')
  ax.set_xlabel('$t$')
  ax.set_ylabel('$C_m/C_m{(0)}$')
  ax.set_xlim(-100,N_CORR)
  ax.legend()

  fig.savefig(f'{output_directory}/correlationfunction.pdf', format = 'pdf', bbox_inches = 'tight')

def blocking_parameter(directory, output_directory, potts_command):
  N_STARTING = 10000
  N_MEASURE = 131072
  N_DECORRELATION = 10
  Path(directory+'/blocking-parameter-run').mkdir(parents=True, exist_ok=True)
  with open(directory+'/blocking-parameter-run/conf-n_starting.txt', 'w') as filehandle:
    filehandle.write(f'{L}\n{SEED+22}\n{N_MEASURE}\n{N_STARTING}\n{N_DECORRELATION}\n{BETA_C}\n')
  subprocess.run([potts_command, directory+'/blocking-parameter-run/conf-n_starting.txt', directory+'/blocking-parameter-run/'])

  m_r, m_c, e = np.loadtxt(directory+f'/blocking-parameter-run/measures.txt', unpack=True)
  m = abs(m_r+ 1j * m_c)
  b_std_m = np.array(Parallel(n_jobs=multiprocessing.cpu_count())(delayed(blocking_std)(m, j) for j in tqdm(range(18))))
  b_std_e = np.array(Parallel(n_jobs=multiprocessing.cpu_count())(delayed(blocking_std)(e, j) for j in tqdm(range(18))))

  fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(10/1.5,7/1.5),sharex=True)

  ax.plot(np.array(range(18)), np.log(b_std_e), lw=0.5, linestyle = '', marker = 'o', label='$\\log{\\sigma_\\epsilon}$')
  ax.plot(np.array(range(18)), np.log(b_std_m), lw=0.5, linestyle = '', marker = 'o', label='$\\log{\\sigma_m}$')
  ax.set_xlabel('$\\log_2{(\\text{block size})}$')
  ax.legend()

  fig.savefig(f'{output_directory}/blocking.pdf', format = 'pdf', bbox_inches = 'tight')

def bootstrap_parameter(directory, output_directory, potts_command):
  N_STARTING = 10000
  N_MEASURE = 100000
  N_DECORRELATION = 10
  BLOCKING_SIZE = 9
  N_RESAMPLE_MAX = 1500
  Path(directory+'/blocking-parameter-run').mkdir(parents=True, exist_ok=True)
  with open(directory+'/blocking-parameter-run/conf-n_starting.txt', 'w') as filehandle:
    filehandle.write(f'{L}\n{SEED}\n{N_MEASURE}\n{N_STARTING}\n{N_DECORRELATION}\n{BETA_C}\n')
  subprocess.run([potts_command, directory+'/blocking-parameter-run/conf-n_starting.txt', directory+'/blocking-parameter-run/'])

  m_r, m_c, e = np.loadtxt(directory+f'/blocking-parameter-run/measures.txt', unpack=True)
  m = abs(m_r+ 1j * m_c)

  bm = blocking(m, BLOCKING_SIZE)
  be = blocking(e, BLOCKING_SIZE)
  b_std_m = np.array(Parallel(n_jobs=multiprocessing.cpu_count())(delayed(bootstrap_std)(lambda x: (2*(L)**2)*np.var(x), m, j) for j in tqdm(range(1,N_RESAMPLE_MAX))))
  b_std_e = np.array(Parallel(n_jobs=multiprocessing.cpu_count())(delayed(bootstrap_std)(lambda x: (2*(L)**2)*np.var(x), e, j) for j in tqdm(range(1,N_RESAMPLE_MAX))))
  fig, (ax1, ax2)= plt.subplots(nrows=2, ncols=1,figsize=(10/1.5,7/1.5))

  ax1.plot(np.arange(1,N_RESAMPLE_MAX), b_std_e, markersize=2.5, linestyle = '', marker = 'o')
  ax1.set_ylabel('$\\sigma_c$')
  ax2.plot(np.arange(1,N_RESAMPLE_MAX), b_std_m, markersize=2.5, linestyle = '', marker = 'o')
  ax2.set_ylabel('$\\sigma_\\chi$')
  ax2.set_xlabel('$\\text{number of resamples}$')

  fig.savefig(f'{output_directory}/bootstrap.pdf', format = 'pdf', bbox_inches = 'tight')

def main():
  directory = sys.argv[1] + '/calibration'
  output_directory = sys.argv[2]
  potts_command = sys.argv[3]

  Path(directory).mkdir(parents=True, exist_ok=True)

  plt.style.use('seaborn')
  plt.rc('text', usetex=True)
  plt.rc('text.latex', preamble='\\usepackage{amsmath} \\usepackage{siunitx}')

  n_strating(directory, output_directory, potts_command)
  correlations_plot(directory, output_directory, potts_command)
  blocking_parameter(directory, output_directory, potts_command)
  bootstrap_parameter(directory, output_directory, potts_command)
  

if __name__ == '__main__':
  main()