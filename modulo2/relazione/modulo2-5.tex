\documentclass[10pt,a4paper]{article}

\usepackage[italian]{babel}
\usepackage[utf8]{inputenc}
\usepackage[paper=a4paper, left=2.5cm, right=2.5cm, top=2.5cm, bottom=2.5cm]{geometry}
\usepackage[section]{placeins}
\usepackage[indent=0cm]{parskip}
\usepackage[colorlinks, linkcolor=blue, urlcolor=magenta, citecolor=teal, bookmarks]{hyperref}
\usepackage{graphicx}
\usepackage{physics}
\usepackage{bbold}
\usepackage{hyperref}
\usepackage{verbatim}
\usepackage{tikz}
\usepackage{amssymb}
\usepackage{amsmath}
\usetikzlibrary{calc}
\usepackage[backend=bibtex]{biblatex}
\addbibresource{bibliography.bib}
\usepackage{caption}
\usepackage{subcaption}

\newcommand{\paginavuota}{\newpage\null\thispagestyle{empty}\newpage}

\begin{document}

%TITOLO


\begin{titlepage}
\title{\textsc{Modello Hubbard con vari metodi di diagonalizzazione}}
\author{Luca Arnaboldi \and Marianna Crupi}

\maketitle

\thispagestyle{empty} 


\end{titlepage} 

\section{Scopo del progetto}

In questo progetto studieremo il modello di Hubbard su reticolo unidimensionale. L'hamiltoniana di interazione che prenderemo in considerazione sarà:
\begin{equation}
	\hat{H} = -t \sum_i \left(\hat{b}^{\dagger}_i\hat{b}_{i+1} + \hat{b}_i\hat{b}^{\dagger}_{i+1}\right) + \frac U2 \sum_i \hat{n}_i\left(\hat{n}_i -1\right) + V \sum_i \hat{n}_i\hat{n}_{i+1}
	\label{hamiltonian}
\end{equation}
dove con $\hat{b}^{\dagger}_i$ e $\hat{b}_i$ abbiamo indicato rispettivamente l'operatore di creazione e distruzione dell'$i$-esimo sito bosonico, con $\hat{n}_i$ l'operatore numero per il sito $i$ e con $t$, $U$ e $V$ le costanti di accoppiamento. Nello sviluppo della relazione useremo la normalizzazione $U=1$. \\
Per grandi valori di $t$ i bosoni saranno completamente delocalizzati e il sistema si troverà in uno stato superfluido. Per piccoli $t$, invece, i bosoni saranno localizzati e il sistema si troverà nello stato di isolante di Mott. Il confine tra le due fasi è dato dall'energia necessaria a rimuovere o aggiungere una particella una volta che il sistema si trovi in uno stato di densità (numero di particelle su numero di siti) $\rho$ intero. Siamo dunque interessati a studiare i potenziali chimici:
\begin{align*}
	&	\mu^p = E^p - E_0 \\
	&	\mu^h = E_0 - E^h
\end{align*}
dove $E_0$ è l'energia della configurazione di base (numero di particelle pari al numero di particelle), $E^p$ l'energia della configurzione con una particella in più rispetto a quella base e $E^h$ l'energia della configurazione con una in meno. Abbiamo assunto condizioni al bordo aperte.\\
In questo progetto ci siamo concentrati sullo studio di un sistema con densitá $\rho = 1$ e $V=0$, salvo dove diversamente specificato.\\
Per studiare questo sistema abbiamo implementato differenti tecniche di diagonalizzazione per l'hamiltoniana:
\begin{itemize}
	\item diagonalizzazione esatta;
	\item algoritmo Lanczos;
	\item finite-system DMRG.
\end{itemize}
Al fine di familiarizzare con gli algoritmi di diagonalizzazione esatta e secondo Lanczos, li abbiamo dapprima applicati per risolvere il ben noto modello di Ising unidimensionale. Per una trattazione più approfondita in merito vedere Appendice \ref{ising}.\\
Tutta l'implementazione numerica è stata svolta utilizzando il linguaggio \texttt{C++}, mentre l'analisi dei dati e i grafici sono stati ottenuti mediante l'uso di \texttt{Python}. In particolare per l'implementazione della diagonalizzazione esatta abbiamo utilizzato la libreria \texttt{Eigen} \cite{eigenweb} e \texttt{Spectra} \cite{spectraweb} per il Lanczos.\\
Il codice utilizzato in questa relazione è consultabile in \href{https://gitlab.com/l.arnaboldi/metodi-numerici}{questa repository}
Passiamo adesso alla descrizione dei vari algoritmi applicati al modello di Hubbard.

\section{Diagonalizzazione esatta e Lanczos}
Come prima soluzione naive al problema abbiamo implementato la diagonalizzazione esatta e tramite Lanczos dell'hamiltoniana in Eq. \eqref{hamiltonian} a fissato massimo numero di occupazione per sito $n_{\text{max}}$.\\
Data la bassa dimensione che questi metodi sono in grado di gestire, in questa relazione il loro impiego è pensato per essere semplicemente un \emph{benchmark} per il finite-size DMRG, senza tentare un'analisi dati basata sui risultati da essi prodotti.

\subsection{Scelta della base}
La base in assoluto più comoda in cui lavorare sarebbe quella che distingue i vari stati in funzione del numero di occupazione di ogni sito. Tuttavia tale base, al contrario del caso del modello di Ising, non ammette un'evidente corrispondente computazionale. \\
Per poter lavorare comunque in tale base, a fissato numero di siti $\ell$, abbiamo dunque operato nel seguente modo:
\begin{itemize}
	\item abbiamo fissato il numero di particelle del modello $N$;
	\item abbiamo implementato una funzione ricorsiva in grado di generare sequenzialmente tutti i possibili stati del sistema;
	\item man mano che gli stati erano creati, venivano numerati sequenzialmente;
	\item dato un numero corrispondendente a un elemento della base, l'associazione allo stato veniva eseguita tramite vettore;
	\item dato uno stato la corrispondenza all'elemento di base veniva eseguita tramite \emph{hash-table} in tempo costante\footnote{Per la forma esplicita della funzione di hash si veda \href{https://stackoverflow.com/a/27216842}{questa referenza}.}.
\end{itemize}

Riportiamo in Figura~\ref{fig:states} la dimensione del sistema al variare di $\ell=N$, da cui si deduce chiaramente un andamento esponenziale. 
In particolare, a paritá di lunghezza della catena, é possibile notare che la dimensione del sistema per il modello di Hubbard risulta molto maggiore rispetto al modello di Ising.
\begin{figure}
  \centering
  \includegraphics[width=0.7\linewidth]{img/states.pdf}
  \caption{Grafico della dimensione del sistema al variare di $\ell=N$, avendo fissato a 4 il numero massimo di occupazione di un sito.}
  \label{fig:states}
\end{figure}

\subsection{Calibrazione di $n_{\text{max}}$}
Successivamente è stata necessaria l'individuazione del minimo $n_{\text{max}}$ che compisse un errore trascurabile nell'algoritmo. Per fare ciò, usando la diagonalizzazione tramite Lanczos, abbiamo calcolato le energie di ground state $E_0$ per una catena con parametri $\ell = N = 10$, $t=0.3$ e $V=0$, aumentando gradualmente il numero massimo di occupazione per sito. Dai risultati riportati in Figura \ref{fig:nmax} risulta evidente che, per avere un errore $<10^{-5}$, è possibile scegliere:
\begin{equation*}
	n_\text{max} = 4.
\end{equation*}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{img/maxoccupationssite.pdf}
  \caption{Grafico dell'energia di ground state $E_0$, trovata tramite Lanczos, al variare di $n_\text{max}$ con parametri $\ell = N = 10$, $t=0.3$ e $V=0$.}
  \label{fig:nmax}
\end{figure}
%\begin{figure}
%	\centering
%%	\vspace{-50pt}
%	\begin{subfigure}{0.66\textwidth}
%		\includegraphics[width=\linewidth]{img/maxoccupationssite.pdf}
%	\end{subfigure}\hfil
%	\begin{subfigure}{0.34\textwidth}
%		\includegraphics[width=\linewidth]{img/maxoccupationssitezoom.pdf}
%	\end{subfigure}
%	\caption{A sinistra il grafico dell'energia di ground state $E_0$, trovata tramite Lanczos, al variare di $n_\text{max}$ con parametri $\ell = N = 8$, $t=0.3$ e $V=0$. A destra lo stesso grafico escludendo a partire da $n_\text{max} = 3$, da cui si nota con maggior chiarezza la stabilità a partire da $n_\text{max} = 4$.}
%	\label{fig:nmax}
%\end{figure}

\subsection{Confronto tra diagonalizzazione esatta e Lanczos}
Abbiamo dunque proceduto con il confronto tra il metodo di diagonalizzazione esatta e il Lanczos.\\
In primo luogo ci siamo concentrati su possibili discrepanze tra i risultati prodotti. In Figura \ref{fig:lvse} è riportata la differenza delle energie di ground state calcolate con i due diversi metodi di diagonalizzazione per $\ell = N$ crescente, fino a un massimo di 8. Risulta evidente che la discrepanza trovata rimane nell'ordine di $10^{-14}$, ovvero comparabile con la precisione maccchina ($10^{-15}$ per i \texttt{double}). Possiamo quindi concludere che i due algoritmi producono risultati in ottimo accordo. \\
\begin{figure}
	\centering
		\includegraphics[width=\linewidth]{img/lanczosvsexact.pdf}
		\caption{Grafico delle differenze dell'energia di ground state $E_0$, trovata tramite Lanczos e tramite diagonalizzazione esatta, al variare di $\ell = N$ con parametri $t=0.3$ e $V=0$.}
	\label{fig:lvse}
\end{figure}
Successivamente abbiamo analizzato la differenza nel tempo di calcolo dei due metodi. Dai risultati riportati in Figura \ref{fig:time} possiamo vedere che già per catene di lunghezza $\ell = 7$ la discrepanza è notevole, con una prestazione nettamente migliore fornita dal Lanczos. In generale il Lanczos risulta sempre più performante della diagonalizzazione esatta, come aspettato. \\
Abbiamo riscontrato inoltre che, sulla macchina utilizzata per le simulazioni, per $\ell = 9$ l'hamiltoniana completa supera la memoria RAM disponibile (16 GB). Non é quindi piú possibile utilizzare la diagonalizzazione esatta, ma solamente tecniche che prevedano l'uso di matrici sparse. Infine, per $\ell \ge 15$ non é piú possibile utilizzare nemmeno il Lanczos per la diagonalizzazione del sistema, in quanto la dimensione dell'\emph{hash-table} supera la memoria dispobilibile; sarebbe necessaria una codifica degli stati piú efficiente in termini di memoria.

\begin{figure}
\centering
\includegraphics[width=\linewidth]{img/timing.pdf}
\caption{Grafico delle differenze del tempo computazionale di Lanczos, diagonalizzazione esatta e DMRG~(2 \emph{sweeps}) al variare di $\ell = N$ con parametri $t=0.3$ e $V=0$.
         I tempi sono stati presi su una CPU \texttt{AMD Ryzen 5 @ 2.9MHz}, senza utilizzare calcolo parallelo.}
\label{fig:time}
\end{figure}

\section{Finite-system DMRG}  
Avendo fissato dei \emph{bench mark} di riferimento siamo passati all'implementazione dell'algoritmo DMRG e in particolare alla versione su stati finiti. Anche in questo caso abbiamo fissato il massimo numero di occupazione $n_\text{max} = 4$, facendo riferimento alla calibrazione effettuata nella sezione precedente tramite Lanczos.

\subsection{Implementazione dell'algoritmo}

Per implementare l'algoritmo abbiamo iniziato definendo la \texttt{struct} del blocco, contenente i seguenti operatori, riferiti al blocco stesso:
\begin{itemize}
	\item l'hamiltoniana;
	\item l'operatore di distruzione relativo al sito estremale $\hat{b}_\text{ext}$;
	\item l'operatore numero relativo al sito estremale $\hat{n}_\text{ext}$;
	\item l'operatore identico relativo al sito estremale $\hat{I}_\text{ext}$.
\end{itemize}
Notiamo che, nonostante sia teoricamente possibile ottenere $\hat{n}_\text{ext}$ da $\hat{b}_\text{ext}^{\dagger}\hat{b}_\text{ext}$, tale operazione risulta sconveniente dal punto di vista computazionale. \\
Per implementare l'algoritmo, abbiamo iniziato costruendo il blocco contente un solo sito. Abbiamo proceduto quindi operando con l'algoritmo infinite-size sfruttando la simmetria per riflessione rispetto al centro della catena, come illustrato a lezione. Una volta giunti al blocco contenente $\lfloor \frac{\ell}2\rfloor$ siti, siamo passati alla versione dell'algoritmo finite-size. \\
Al fine di studiare un sistema a numero di particelle $N$ fissato, è stata necessaria l'implementazione nel codice di una simmetria abelliana. In particolare per tenere traccia del numero di occupazione degli stati del sistema ai vari step, all'hamiltoniana di ogni blocco contenente $n$ siti abbiamo associato il vettore $\vec{v}^{(n)}$ avente in posizione $i$ il numero di particelle dell'$i$-esimo stato del blocco. Per fare ciò:
\begin{itemize}
	\item per il blocco costituito da un sito abbiamo utilizzato la base del numero di occupazione, ordinata in maniera crescente;
	\item abbiamo associsto a tale blocco il vettore $\vec{u}^{(1)} = (0, \ldots, n_\text{max})$;
	\item abbiamo proceduto quindi alla costruzione del blocco allargato, includendo il primo sito sulla destra, con vettore di particelle $\vec{w}$. Abbiamo aggiornato quindi tutti gli operatori di blocco, operando il prodotto tensore tra gli operatori relativi al nuovo sito e quelli relativi al blocco precedente;
	\item abbiamo ottenuto il nuovo vettore di particelle dello spazio costituito dal blocco più il link come $\vec{v}^{(1)}= \left(u^{(1)}_1+\vec{w}, \ldots, u^{(1)}_{n_\text{max}}+\vec{w}\right)$;
	\item abbiamo creato l'hamiltoniana di super-blocco $\hat{H}_{SB}$, operando il prodotto tensore tra il sistema costituito dal blocco più il link e se stesso riflesso (nel caso infinito), o il blocco della lunghezza coretta preso dalla memoria (nel caso finito);
	\item tale hamiltoniana risulta diagonale a blocchi con numero di particelle fissato, a meno di un riordinamento degli stati;
	\item abbiamo operato il riordinamento necessario a ottenere una struttura a blocchi $\hat{R}^{\dagger}\hat{H}_{SB}\hat{R}$, dove con $\hat{R}^{\dagger}$ abbiamo indicato la matrice che data la base originale restituisce quella ordinata nel numero di particelle;
	\item abbiamo estratto il solo blocco corrispondente al numero di particelle a cui siamo interessati: per l'infinite-size DMRG prenderemo il blocco corrispondente a $\lfloor \frac{\text{\# siti di SB}}{\ell}N\rfloor$, mentre per il finite-size DMRG sarà semplicemente $N$;
	\item abbiamo trovato il ground state $\ket{\psi_0}$ di tale matrice sparsa (sparsità $< 0.06$), mediante la tecnica del Lanczos;
	\item abbiamo riportato $\ket{\psi_0}$ nella base disordinata ottenendo $\ket{\tilde{\psi}_0} = \hat{R} \ket{\psi_0}$;
	\item abbiamo proceduto quindi a eseguire la traccia parziale di $\ket{\tilde{\psi}_0} \bra{\tilde{\psi}_0}$ sul blocco di destra. Per fare ciò abbiamo iniziato operando con la funzione \texttt{reshape}, ottenendo una matrice $\hat{\rho}'$ con numero di colonne pari al numero di siti del blocco di sinistra e numero di righe pari a quello di destra. Successivamente abbiamo ottenuto la corretta matrice densità ridotta del blocco ad $n$ siti come: $\hat{\rho}^{(n)} = \hat{\rho}'^{\dagger}\hat{\rho}'$;
	\item notiamo che, per la procedura utilizzata, anche $\hat{\rho}^{(n)}$ sarà a blocchi a meno di riordinamento e che la matrice di cambio base $\hat{R}^{\dagger}$ e il vettore di particelle $\vec{v}^{(n)}$ associati al super-blocco a $2n$ siti saranno li stessi anche per $\hat{\rho}^{(n)}$;
	\item abbiamo dunque operato il cambio base che rende $\hat{\rho}^{(n)}$ a blocchi di numero di particelle fissato:  $\hat{R}\hat{\rho}^{(n)}\hat{R}^{\dagger}$;
	\item abbiamo operato la diagonalizzazione dell'intera matrice, blocco per blocco, utilizzando la diagonalizzazione esatta in quanto non siamo in presenza necessariamente di matrici sparse;
	\item abbiamo operato il cambio base inverso, per tornare alla base disordinata, tenendo traccia della corrispondenza tra autovettori e autovalori;
	\item abbiamo costruito l'operatore di troncamento, in modo da ridurre la matrice così ottenuta ai soli sottospazi corrispondenti agli autovalori maggiori;
	\item concordamente abbiamo aggiornato il vettore di particelle corrispondente;
	\item abbiamo operato lo stesso troncamento anche a tutti gli operatori del blocco.
\end{itemize}
Procedendo in questo modo, operando diverse \emph{sweeps} come descritto a lezione, siamo riusciti a implementare il finite-size DMRG con numero di particelle fissato.

\subsection{Calibrazione numero di sweep}
Abbiamo dunque proceduto con l'individuazione del minimo numero di \emph{sweep} necessarie all'algoritmo per stabilizzarsi. Per fare ciò abbiamo calcolato le energie di ground state $E_0^\text{(sweep)}$ per una catena con parametri $\ell = N = 10$, $t=0.3$ e $V=0$, scegliendo per ogni \emph{sweep} il valore centrale corrispondente al blocco di siti $\lfloor \frac{\ell}2\rfloor$. Per fare ciò abbiamo usato come figura di merito il logaritmo dell'errore relativo calcolato come:
\begin{equation*}
	\log \left(\frac{E_0^\text{(sweep)}-E_0^{(10)}}{E_0^{(10)}}\right)
\end{equation*}
dove con $E_0^{(10)}$ indichiamo il valore del ground state dopo 10 \emph{sweeps}, che possiamo considerare ragionevolmente come il valore effettivo. \\
Dai risultati riportati in Figura \ref{fig:sweeps} risulta evidente che è possibile scegliere:
\begin{equation*}
	n_\text{sweeps} = 2.
\end{equation*}

\begin{figure}
	\centering
		\includegraphics[width=\linewidth]{img/sweeps.pdf}
	\caption{Grafico del logaritmo dell'errore relativo per l'energia di ground state al variare del numero di \emph{sweeps}, con parametri $\ell = N = 10$, $t=0.3$ e $V=0$.}
	\label{fig:sweeps}
\end{figure}

\subsection{Calibrazione troncamento}
Per quanto riguarda la scelta della dimensione dell'operatore di troncamento $m$, abbiamo scelto di porre
\begin{equation*}
  m = 20
\end{equation*}
in quanto è l'ultima dimensione che ci permette di avere un tempo di calcolo gestibile. In particolare una simulazione condotta per $m= 30$ ha mostrato una riduzione dell'errore di un solo ordine di grandezza, in concomitanza di un aumento del tempo di calcolo di un fattore $4$. \\
Ci si potrebbe aspettare che, all'aumentare delle dimensioni della catena, aumenti anche il massimo errore commesso a causa del troncamento. Tuttavia l'andamento riportato in Figura \ref{fig:troncamento} mostra che tale errore raggiunge invece un \textit{plateau} al crescere della dimensione. Nell'analisi seguente abbiamo quindi considerato valide simulazioni fino a $\ell = 250$. 
\begin{figure}
  \centering
  \includegraphics[width=0.7\linewidth]{img/truncationerrortwentylinear.pdf}
  \caption{Grafico del massimo errore commesso con troncamento $m=20$, all'aumentare di $\ell$, con $t=0.3$ e $V=0$.}
  \label{fig:troncamento}
\end{figure}


\subsection{Confronto tra DMRG e Lanczos}
Abbiamo dunque proceduto analizzando possibili discrepanze con l'algoritmo Lanczos.\\
In Figura \ref{fig:lvsDMRG} sono riportati i logaritmi delle differenze delle energie di ground state calcolate con i due diversi metodi di diagonalizzazione, riscalati per l'energia ottenuta con l'algoritmo DMRG, per $\ell$ crescente, fino a un massimo di 8 e $N$ rispettivamente di $\ell-1$, $\ell$, $\ell+1$.
La discrepanza trovata rimane nell'ordine di $10^{-8}$. Ipotizzando dunque un andamento al più lineare in $\ell$, simulando reticoli con $\ell \sim 10^2$, ci aspettiamo una discrepanza dell'ordine di $10^{-6}$, che risulta inferiore all'errore commesso fissando $n_\text{max} = 4$. Possiamo quindi concludere che i due algoritmi producono risultati in ottimo accordo.\\
Successivamente abbiamo analizzato la differenza nel tempo di calcolo dei due metodi. Dai risultati riportati in Figura \ref{fig:time} é possibile notare che il DMRG, dopo un transiente iniziale di crescita esponenziale, si assesta su un andamento lineare nel numero di siti. Per i paremetri scelti e sulla machina utilizzata, dunque, il DMRG risulta definitivamente piú veloce del Lanczos a partire da $\ell = 14$.\\
\begin{figure}
	\centering
	\begin{subfigure}{0.7\textwidth}
		\includegraphics[width=\linewidth]{img/lanczosvsdmrghole.pdf}
	\end{subfigure}\hfil
  \medskip
	\begin{subfigure}{0.7\textwidth}
		\includegraphics[width=\linewidth]{img/lanczosvsdmrgground.pdf}
	\end{subfigure}
	\medskip
	\begin{subfigure}{0.7\textwidth}
		\includegraphics[width=\linewidth]{img/lanczosvsdmrgparticle.pdf}
	\end{subfigure}
	\caption{Grafico del logaritmo delle differenze dell'energia di ground state $E_0$, trovata tramite DMRG e tramite Lanczos, al variare di $\ell$ con parametri $t=0.3$, $V=0$ e $N$ rispettivamente  $\ell-1$, $\ell$, $\ell+1$.}
	\label{fig:lvsDMRG}
\end{figure}


\subsection{Studio della transizione di fase}
Abbiamo infine applicato l'algoritmo descritto nelle sezioni precedenti allo studio dei potenziali chimici al fine di individuare la transizione di fase del sistema da isolante di Mott a liquido super conduttore.\\
Per fare ciò è stato necessario innanzitutto determinare i potenziali chimici a dato $t$. A tale scopo, come suggerito dall'articolo \cite{DMRG}, abbiamo eseguito l'algoritmo e calcolato i potenziali chimici per $\ell = 10, 20, 40, 60, 80, 100, 160, 250$. L'andamento dei potenziali in funzione di $\ell^{-1}$, ottenuto per $t=0.31$ è riportato in Figura \ref{fig:fitmu} a titolo di esempio.\\ %Dalla figura risulta evidente che i valori dei potenziali calcolati per $\ell \geq 160$ presentano un errore troppo elevato, dovuto a un troncamento eccessivo rispetto alla dimensione del sistema, e sono dunque stati scartati nell'analisi seguente. \\
\begin{figure}
	\centering
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\linewidth]{img/singlepointfithole.pdf}
	\end{subfigure}\hfil
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\linewidth]{img/singlepointfitparticle.pdf}
	\end{subfigure}
	\caption{Grafico del potenziale chimico di buca (a sinistra) e di particella (a destra), al variare di $\ell^{-1}$, con $t=0.31$ e $V=0$.}
	\label{fig:fitmu}
\end{figure}
Come riportato nell'articolo \cite{DMRG}, è possibile notare che l'andamento dei potenziali per $V=0$ si discosta sensibilmente dall'andamento lineare. Abbiamo dunque deciso di eseguire il \textit{fit} tenendo conto prima del solo termine lineare, successivamente anche di quello quadratico e infine del termine cubico. Dai risultati ottenuti abbiamo notato una differenza sensibile tra il \textit{fit} lineare e quello quadratico, mentre nessuna differenza tra quest'ultimo e quello cubico. Abbiamo dunque deciso, per una maggiore precisione, di procedere nell'analisi utilizzando una funzione di \textit{fit} quadratica. \\
Abbiamo dunque operato il \emph{fit} facendo uso della seguente:
\begin{equation*}
	\mu^{h,p}_{\ell} = a\cdot \ell^{-2} + b \cdot \ell^{-1} + \mu^{h,p}
\end{equation*}
lasciando come parametri liberi $a$, $b$ e $\mu^{h,p}$. Abbiamo infine considerato il parametro $\mu^{h,p}$ trovato come effettivo potenziale di buca e particella rispettivamente.\\
Avendo quindi determinato i potenziali chimici a dato $t$, come descritto in precedenza, per ritrovare la struttura a lobi abbiamo applicato quanto sopra descritto per 72 valori di $t$ equispaziati tra $0$ e $0.4$. I risultati riportati in Figura~\ref{fig:mu} riproducono in maniera soddisfacente l'andamento aspettato.
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{img/lobes.pdf}
	\caption{Grafico del potenziale chimico di buca (in rosso) e di particella (in verde), al variare di $t$ tra $0$ e $0.4$, con $V=0$.}
	\label{fig:mu}
\end{figure}
In ultima analisi abbiamo determinato il valore $t_c$ a cui avviene la transizione di fase, ovvero il punto in cui i potenziali chimici sono uguali (a meno dell'errore numerico). Nella Figura~\ref{fig:diffmu} mostriamo il grafico della differenza (rinormalizzata a due volte la media) tra i due potenziali chimici al variare di $t$.
\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{img/lobesdifference.pdf}
  \caption{Grafico della differenza tra potenziale chimico di buca e di particella, rinormalizzata a due volte la media dei potenziali, al variare di $t$ tra $0$ e $0.4$, con $V=0$.}
  \label{fig:diffmu}
\end{figure}
Il grafico mostra che tale differenza si riduce al crescere di $t$, fino al raggiungimento di un minimo, per poi assumere un comportamento anomalo, chiaro segnale dell'avvenuta transizione di fase. Abbiamo dunque scelto come $t_c$ il valore di $t$ per cui si realizza il minimo della differenza, prima del comportamento anomalo: 
\[t_c = 0.310\pm0.006.\]
Tale valore risulta compatibile con quanto ottenuto in \cite{DMRG} per alcuni valori del parametro di correlazione $r$ ($\Gamma(r) = \langle b^{\dagger}_rb_0 \rangle$). In particolare:
\[
	t_c = 0.3062 \pm 0.0003 \qquad \text{per} \quad 32 \le r \le 48,
\]
\[
	t_c = 0.3107 \pm 0.01 \qquad \text{per} \quad 48 \le r \le 64.
\]
Inoltre il valore da noi ottenuto risulta compatibile anche con quanto svolto nei lavori \cite{primovalore} e \cite{secondovalore}, che hanno ottenuto rispettivamente:
\[
	t_c = 0.300 \pm 0.005, 
\]
\[
	tc = 0.304 \pm 0.002.
\]

%La differenza torna poi ad essere rilevante, probabilmente perché la fase superfluida richiederebbe molte piú risorse per essere simulata propriamente.

 

\subsection{Possibili implementazioni ulteriori}
Per come è stato scritto, il nostro codice è in grado di lavorare anche a $V \ne 0$ e $\rho \ne 1$. \\
A dimostrazione di ciò, operando in maniera del tutto analoga a quanto illustrato in precedenza, abbiamo prodotto, a scopo illustrativo, il grafico dei potenziali chimici per $V = 0.4$ e $\rho = 1$. Il risultato è riportato in Figura \ref{fig:esotico} ed è possibile riscontrare l'andamento a lobi, come atteso dal confronto con \cite{DMRG}. \\
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{img/lobeswithbound.pdf}
	\caption{Grafico del potenziale chimico di buca e di particella, al variare di $t$ tra $0$ e $0.45$, con $V=0.4$. Il grafico è stato ottenuto con \textit{convergence speed} (si veda Appendice \ref{ising}) della libreria \texttt{Spectra} ridotta rispetto alle analisi precedenti, in quanto la RAM a nostra disposizione non sarebbe stata altrimenti sufficiente.}
	\label{fig:esotico}
\end{figure}
Contrariamente a quanto atteso, tuttavia, i due potenziali chimici sembrano non incontrarsi, probabilmente a causa della presenza di effetti di bordo. Per tentare di tenere conto di tali effetti, seguendo quanto riportato nell'articolo \cite{DMRG}, abbiamo operato una seconda analisi, aggiungendo all'hamiltoniana \eqref{hamiltonian} il termine:
\begin{equation}
	\hat{H}_{OBC} = V\rho \left(\hat{n}_1 + \hat{n}_L\right).
	\label{bound}
\end{equation}
I risultati riportati in Figura \ref{fig:withbound} mostrano un andamento differente da quanto atteso. In effetti analizzando il termine \eqref{bound} è possibile notare che esso introduce  effetti troppo marcati che spostano sensibilmente l'energia della configurazione base, falsando tutte le analisi successive.
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{img/lobeswitthoutbound.pdf}
	\caption{Grafico del potenziale chimico di buca e di particella, al variare di $t$ tra $0$ e $0.45$, con $V=0.4$ e l'aggiunta del termine di bordo \eqref{bound}.}
	\label{fig:withbound}
\end{figure}

\clearpage
\appendix
\section{Ising 1D}
\label{ising}

Per familiarizzare gli algoritmi di diagonalizzazione esatta e Lanczos abbiamo innanzitutto studiato un modello più semplice e ben noto in letteratura: il modello di Ising qunantistico 1D. L'hamiltoniana che abbiamo preso in considerazione è:
\begin{equation}
	\hat{H} = -J \sum_i \hat{\sigma}_i^z\hat{\sigma}_{i+1}^z - h \sum_i \hat{\sigma}_i^x - \lambda \sum_i \hat{\sigma}_i^z
	\label{ham:ising}
\end{equation}
Dove le matrici $\hat{\sigma}$ sono le usuali matrici di Pauli, $J$ è l'intensità della costante di accoppiamento, $h$ e $\lambda$ le intensità del campo trasverso e longitudinale rispettivamente. Considereremo una catena di spin lunga $L$ e normalizzeremo l'hamiltoniana \eqref{ham:ising} per $J=1$.\\
Di questo sistema le quantità fisicamente interessanti sono:
\begin{align*}
	\langle \hat{M}^z\rangle  &= \bra{\psi_0} \hat{M}^z \ket{\psi_0} = \bra{\psi_0} \sum_i \hat{\sigma}_i^z \ket{\psi_0}; \\
	\langle \hat{M}^x\rangle  &= \bra{\psi_0} \hat{M}^x \ket{\psi_0} = \bra{\psi_0} \sum_i \hat{\sigma}_i^x \ket{\psi_0}; \\
	C^{zz} & = \bra{\psi_0} \hat{\sigma}_{\lfloor \frac L2 \rfloor}^z\hat{\sigma}_{\lfloor \frac L2 \rfloor+1}^z \ket{\psi_0}; \\
	E_G & = E_1 - E_0.
\end{align*}
Ovvero, rispettivamente, la magnetizzazione media lungo $z$, lungo $x$, la correlazione tra i due stati consecutivi centrali della catena e il gap energetico tra ground state e primo eccitato. \\
Numericamente, tuttavia, la quantità $\langle \hat{M}^z \rangle$ risulta sempre nulla, a causa della degenerazione del ground state. Dato dunque un ground state scomposto sulla base computazionale come $\ket{\psi_0} = \sum_n \alpha_n \ket{n}$, saremo invece interessati alla quantità:
\begin{equation}
	\tilde{M}^z = \sum_n \abs{\alpha_n}^2 \abs{\bra{n} \sum_j \hat{\sigma}_j \ket{n}}
\end{equation}
Anche in questo caso abbiamo implementato il codice in linguaggio \texttt{C++}, mentre abbiamo eseguito l'analisi dati facendo ricorso a \texttt{Python}. \\
Per l'implementazione della diagonalizzazione esatta abbiamo utilizzato la libreria \texttt{Eigen} \cite{eigenweb}. In particolare abbiamo utilizzato questo metodo, computazionalmente molto costoso, solamente come confronto per la tecnica approssimata del Lanczos, di cui passiamo adesso ad una descrizione dettagliata.

\subsection{Lanczos}
Per l'implementazione del Lanczos abbiamo confrontato diverse librerie al fine di individuare la più adatta ai nostri scopi. In particolare abbiamo studiato:
\begin{itemize}
	\item \texttt{Lambda Lanczos} \cite{lambdalanczosweb};
	\item \texttt{Spectra} \cite{spectraweb}.
\end{itemize}
Passiamo adesso ad un'anallisi più dettagliata per ciascuna.

\subsubsection{\texttt{Lambda Lanczos}}
La libreria \texttt{Lambda Lanczos} \cite{lambdalanczosweb} è in grado di calcolare la solo coppia autovalore-autovettore relativa all'autovalore di modulo minore. Al fine di trovare i tre autovalori (con corrispettivi autovettori), è stato necessario operare nel seguente modo:
\begin{itemize}
	\item per assicurarci che il ground state fosse anche l'autovalore di \emph{modulo} minore, era necessario \emph{shiftare} tutti gli autovalori in modo da renderli tutti positivi;
	\item l'algoritmo forniva dunque autovalore e autovettore relativi al ground state del sistema;
	\item a questo punto risultava necessario proiettare l'autospazio relativo all'autovettore appena trovato in 0\footnote{Sottolineiamo che, dopo aver eseguito la proiezione indicata, gli autovettori della nuova matrice risulteranno modificati, tuttavia non gli autovalori. Non essendo interessati al valore degli autovettori, abbiamo proeduto con tale metodo. Per approfondire si veda \href{https://math.stackexchange.com/questions/1114777/approximate-the-second-largest-eigenvalue-and-corresponding-eigenvector-given}{questa referenza}.};
	\item iterare la procedura altre due volte, ottenendo così i primi tre livelli energetici del sistema. In questo modo risulta possibile lo studio dell'\textit{energy gap} anche in presenza di un ground state degenere.
\end{itemize}
%Operando come descritto sopra, il valore ottenuto per l'autovalore del ground state degenere, non era in realtà corrispondente con l'autovalore del ground state. Tuttavia il valore dell'energia del primo stato eccitato risultava corretto. \\
In generale le prestazioni fornite da tale metodo risultano poco soddisfacenti in quanto l'effeettuazione dello \emph{shift} causa una perdita di precisione numerica.

\subsubsection{\texttt{Spectra}}
Abbiamo successivamente effettuato l'implementazione tramite la libreria \texttt{Spectra} \cite{spectraweb}. Per il suo utilizzo è stato necessario semplicemente scegliere l'autovalore da fornire (nel nostro caso il più piccolo algebricamente) e la \emph{convergence speed} che regola la quantità di risorse che all'algoritmo è concesso utilizzare. \\
%Anche con l'utilizzo di questa libreria abbiamo riscontrato l'inesattezza dell'autovalore relativo al ground state degenere. \\
Dai test effettuati, la libreria \texttt{Spectra} è risultata più stabile numericamente e di più semplice utilizzo. Abbiamo dunque scelto di utilizzare questa libreria per le successive analisi nel modello di Hubbard.


\subsection{Confronto numerico}
Per verificare il corretto funzionamento degli algoritmi utilizzati, abbiamo confrontato i risultati ottenuti con il codice fornito a lezione. Dai valori riportati in Tabella \ref{tab:confronto} possiamo notare un ottimo accordo.

\begin{table}[h]
	\begin{center}
		\begin{tabular}{ccccc}
			\hline
			\hline
					& Esatta	& \texttt{Lambda Lanczos}	& \texttt{Spectra}	& \texttt{Fortran Lapack} \\
			\hline
			$E_0$							& -20.81875104	& -20.81875104	& -20.81875104	&	 	-20.818751039065560	\\
			$E_1$							& -16.66123754	& -16.66123716	& -16.66123754	&	-16.661237541797409 \\
			$E_G$							& 4.15751423	& 4.15751384	& 4.15751350	& 4.1575134973	\\
			$\tilde{M}^z$					& 0.93008010	& 0.93008015	& 0.93008008	& 0.93008009882622544	\\
			$\langle \tilde{M}^z \rangle$	& 0.36047302	& 0.36047288	& 0.36047163	& non fornito	\\
			\hline
			\hline
		\end{tabular}
	\end{center}
	\caption{Confronto dei primi tre valori dell'energia del sistema, della magnetizzazione lungo $x$ e lungo $z$, tra i diversi metodi di diagonalizzazione utilizzati e il codice fornito a lezione. Le simulazioni sono state effettuate con parametri: $L=10$, $\lambda = h = 1$ e condizioni al bordo aperte.}
	\label{tab:confronto}
\end{table} 

%
%ben noti risultati analitici. In particolare, per un'hamiltoniana del tipo:
%\[
%	\hat{H} = - \sum_i \hat{\sigma}_i^z\hat{\sigma}_{i+1}^z -  \sum_i \hat{\sigma}_i^x %- \lambda \sum_i \hat{\sigma}_i^z
%\]
%con condizioni al contorno aperte, sono note le quantità nel limite termodinamico:
%\[
%	E_0 = 1-\frac 1{\sin\left(\frac{\pi}{2(2L+1)}\right)} \\
%	M^x = \frac 2{\pi} \qquad \text{per} L \rightarrow \infty       
%\]




\clearpage
\printbibliography


\end{document}
