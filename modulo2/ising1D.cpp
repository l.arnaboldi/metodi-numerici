#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <random>
#include <string>
#include <utility>
#include <complex>
#include <tuple>

using namespace std;

#include <Eigen/Dense>

using namespace Eigen;

#include <Spectra/MatOp/SparseSymMatProd.h>
#include <Spectra/SymEigsSolver.h>
#include <Spectra/Util/SelectionRule.h>

using namespace Spectra;

#include <lambda_lanczos.hpp>

using lambda_lanczos::LambdaLanczos;

using Real = double;
using Operator = Matrix<Real, Eigen::Dynamic, Eigen::Dynamic>;
using SparseOperator = SparseMatrix<Real>;

inline unsigned int computational_base_spin(unsigned int cb, unsigned int k) {
  return (cb >> k) % 2;
}

inline unsigned int bit_flip(unsigned int cb, unsigned int k) {
  return (1<<k)^cb;
}

Real computational_base_Zmagnetization(unsigned int cb, unsigned ell) {
  int magn = 0;
  for (unsigned j = 0; j < ell; j++,cb >>= 1) {
    if (cb % 2 == 0) {
      magn += 1;
    } else {
      magn -= 1;
    }
  }
  return Real(magn);
}

Real computational_base_Xmagnetization(unsigned int cb, unsigned ell) {
  int magn = 0;
  for (unsigned j = 0; j < ell; j++,cb >>= 1) {
    if (cb % 2 == 0) {
      magn += 1;
    } else {
      magn -= 1;
    }
  }
  return Real(magn);
}

Real computational_base_ZZcorrelation(unsigned int cb, unsigned int i, unsigned int j) {
  if (computational_base_spin(cb, i) == computational_base_spin(cb, j)) {
    return Real(1.);
  } else {
    return Real(-1.);
  }
}

template<typename Value>
Real vector_norm(vector<Value>& v) {
  Real sqn = 0.;
  for (auto& v_: v) {
    sqn += abs(v_)*abs(v_);
  }
  return sqrt(sqn);
}

tuple<Real, Real, Real, vector<complex<Real>>> naive_diagonalization(unsigned int ell, Real h_field, Real lambda_field, unsigned int pbc) {
  unsigned int dimension = 1 << ell;
  Operator hamiltonian(dimension, dimension);
  hamiltonian = Operator::Zero(dimension, dimension);

  // Coupling
  for (unsigned int i = 0; i < dimension; i++) {
    for (unsigned int j = 0; j < ell-1; j++) {
      if (computational_base_spin(i, j) == computational_base_spin(i, j+1)) {
        hamiltonian(i,i) -= 1.;
      } else {
        hamiltonian(i,i) += 1.;
      }
    }
    if (pbc == 1) {
      if (computational_base_spin(i, 0) == computational_base_spin(i, ell-1)) {
        hamiltonian(i,i) -= 1.;
      } else {
        hamiltonian(i,i) += 1.;
      }
    }
  }

  // Transversal field
  for (unsigned int i = 0; i < dimension; i++) {
    for (unsigned int j = 0; j < ell; j++) {
      if (computational_base_spin(i,j) == 1) {
        hamiltonian(i,i) -= lambda_field;
      } else {
        hamiltonian(i,i) += lambda_field;
      }
    }
  }

  // Longitudinal field
  for (unsigned int i = 0; i < dimension; i++) {
    for (unsigned int j = 0; j < ell; j++) {
      hamiltonian(bit_flip(i, j), i) -= h_field;
    }
  }

  //cout << hamiltonian << endl;

  Eigen::EigenSolver<Operator> eigen_solver(hamiltonian);
  vector<Real> eigenvalues;
  for (auto& ev: eigen_solver.eigenvalues()) {
    eigenvalues.push_back(real(ev));
  }

  vector<unsigned> eigenindexes(dimension);
  for (unsigned x = 0; x < dimension; x++) {
    eigenindexes[x] = x;
  }
  // nth_element(
  //   eigenindexes.begin(),
  //   eigenindexes.begin()+1,
  //   eigenindexes.end(),
  //   [eigenvalues](const unsigned& a, const unsigned& b) {return eigenvalues[a]<eigenvalues[b];}
  // );
  // unsigned int ground_state_index = eigenindexes[0];

  // nth_element(
  //   eigenindexes.begin(),
  //   eigenindexes.begin()+2,
  //   eigenindexes.end(),
  //   [eigenvalues](const unsigned& a, const unsigned& b) {return eigenvalues[a]<eigenvalues[b];}
  // );
  // 

  // nth_element(
  //   eigenindexes.begin(),
  //   eigenindexes.begin()+3,
  //   eigenindexes.end(),
  //   [eigenvalues](const unsigned& a, const unsigned& b) {return eigenvalues[a]<eigenvalues[b];}
  // );
  sort(
    eigenindexes.begin(),
    eigenindexes.end(),
    [eigenvalues](const unsigned& a, const unsigned& b) {return eigenvalues[a]<eigenvalues[b];}
  );
  unsigned int ground_state_index = eigenindexes[0];
  unsigned int second_ground_state_index = eigenindexes[1];
  unsigned int first_excited_index = eigenindexes[2];

  vector<complex<Real>> ground_state;
  Real eigenvector_norm = eigen_solver.eigenvectors().col(ground_state_index).norm();
  //clog << "The norm of the computed eigenvector is: " << eigenvector_norm;
  for (auto& c_i: eigen_solver.eigenvectors().col(ground_state_index)) {
    ground_state.push_back(c_i/eigenvector_norm);
  }

  return make_tuple(
    eigenvalues[ground_state_index],
    eigenvalues[second_ground_state_index],
    eigenvalues[first_excited_index],
    ground_state
  );
}

tuple<Real, Real, Real, vector<complex<Real>>> lanczos(unsigned int ell, Real h_field, Real lambda_field, unsigned int pbc) {
  unsigned int dimension = 1 << ell;

  auto H_on_ket = [ell, dimension, h_field, lambda_field](const vector<complex<Real>>& in, vector<complex<Real>>& out) {
    // Coupling
    for (unsigned i = 0; i < dimension; i++) {
      for (unsigned int j = 0; j < ell-1; j++) {
        if (computational_base_spin(i, j) == computational_base_spin(i, j+1)) {
          out[i] -= in[i];
        } else {
          out[i] += in[i];
        }
      }
      // Transversal field
      for (unsigned int j = 0; j < ell; j++) {
        if (computational_base_spin(i,j) == 1) {
          out[i] -= lambda_field * in[i];
        } else {
          out[i] += lambda_field * in[i];
        }
      }
      // Longitudinal field
      for (unsigned int j = 0; j < ell; j++) {
        out[bit_flip(i, j)] -= h_field * in[i];
      }
    }
  };

  LambdaLanczos<complex<Real>> engine(H_on_ket, dimension, false); // false means to calculate the lowest eigenvalue.
  auto [c_ground_state_energy, ground_state, itern] = engine.run();
  for (auto& v: ground_state) {
    v /= vector_norm(ground_state); // renormalizing eigenstate
  }

  // We are using this: https://math.stackexchange.com/questions/1114777/approximate-the-second-largest-eigenvalue-and-corresponding-eigenvector-given
  // NB: eigenvectors of the modified hamiltonian are not eigenvector of the orginial. But we are interested only in eigenvalues, so it does not matter.

  // Second Smallest Eigenvalue
  auto Hone_on_ket = [dimension, H_on_ket, ground_state, c_ground_state_energy](const vector<complex<Real>>& in, vector<complex<Real>>& out) {
    H_on_ket(in, out);
    complex<Real> projection = 0.;
    for (unsigned i = 0; i < dimension; i++) {
      projection += conj(ground_state[i])*in[i];
    }
    for (unsigned i = 0; i < dimension; i++) {
      out[i] -= c_ground_state_energy * ground_state[i] * projection;
    }
  };

  LambdaLanczos<complex<Real>> engine_one(Hone_on_ket, dimension, false);
  engine_one.eigenvalue_offset = c_ground_state_energy * 1.05;
  auto [c_second_ground_state_energy, virtual_eigenvector_one, itern_one] = engine_one.run();
  for (auto& v: virtual_eigenvector_one) {
    v /= vector_norm(virtual_eigenvector_one); // renormalizing eigenstate
  }

  // Third Smallest Eigenvalue
  auto Htwo_on_ket = [dimension, Hone_on_ket, virtual_eigenvector_one, c_second_ground_state_energy](const vector<complex<Real>>& in, vector<complex<Real>>& out) {
    Hone_on_ket(in, out);
    complex<Real> projection = 0.;
    for (unsigned i = 0; i < dimension; i++) {
      projection += conj(virtual_eigenvector_one[i])*in[i];
    }
    for (unsigned i = 0; i < dimension; i++) {
      out[i] -= c_second_ground_state_energy * virtual_eigenvector_one[i] * projection;
    }
  };

  LambdaLanczos<complex<Real>> engine_two(Htwo_on_ket, dimension, false);
  engine_two.eigenvalue_offset = c_ground_state_energy*1.05;
  auto [c_first_excited_energy, virtual_eigenvector_two, itern_two] = engine_two.run();
  for (auto& v: virtual_eigenvector_two) {
    v /= vector_norm(virtual_eigenvector_two); // renormalizing eigenstate
  }

  clog << setprecision(15) << c_ground_state_energy << endl << c_second_ground_state_energy << endl << c_first_excited_energy << endl;

  return make_tuple(
    real(c_ground_state_energy),
    real(c_second_ground_state_energy),
    real(c_first_excited_energy),
    ground_state
  );
}

tuple<Real, Real, Real, vector<complex<Real>>> lanczos_spectra(unsigned int ell, Real h_field, Real lambda_field, unsigned int pbc) {
  unsigned int dimension = 1 << ell;
  SparseOperator hamiltonian = SparseOperator(dimension, dimension);

  // Coupling
  for (unsigned int i = 0; i < dimension; i++) {
    int H_ii = 0;
    for (unsigned int j = 0; j < ell-1; j++) {
      if (computational_base_spin(i, j) == computational_base_spin(i, j+1)) {
        hamiltonian.coeffRef(i, i) -= 1.;
      } else {
        hamiltonian.coeffRef(i, i) += 1.;
      }
    }
    if (pbc == 1) {
      if (computational_base_spin(i, 0) == computational_base_spin(i, ell-1)) {
        hamiltonian.coeffRef(i, i) -= 1.;
      } else {
        hamiltonian.coeffRef(i, i) += 1.;
      }
    }
  }

  // Transversal field
  for (unsigned int i = 0; i < dimension; i++) {
    int H_ii = 0;
    for (unsigned int j = 0; j < ell; j++) {
      if (computational_base_spin(i,j) == 1) {
        hamiltonian.coeffRef(i, i) -= lambda_field;
      } else {
        hamiltonian.coeffRef(i, i) += lambda_field;
      }
    }
  }

  // Longitudinal field
  for (unsigned int i = 0; i < dimension; i++) {
    for (unsigned int j = 0; j < ell; j++) {
      hamiltonian.coeffRef(bit_flip(i, j), i) += -h_field;
    }
  }

  //cout << hamiltonian << endl;

  SparseSymMatProd<Real> hamiltonian_operator(hamiltonian);
  SymEigsSolver<SparseSymMatProd<Real>> engine(hamiltonian_operator, 3, min(100, int(hamiltonian.rows())));
  engine.init();
  int nconv = engine.compute(SortRule::SmallestAlge, 10000);
  if(engine.info() != CompInfo::Successful) {
    cerr << "Unable to compute eigenvalues of reduced H_superblock" << endl;
    exit(1);
  }

  vector<complex<Real>> ground_state;
  Real eigenvector_norm = engine.eigenvectors().col(2).norm();
  for (auto& c_i: engine.eigenvectors().col(2)) {
    ground_state.push_back(c_i/eigenvector_norm);
  }

  return make_tuple(
    engine.eigenvalues()[2],
    engine.eigenvalues()[1],
    engine.eigenvalues()[1],
    ground_state
  );
}

void analisys(Real ground_energy, Real ground_energy_degeneration, Real first_excited_energy, vector<complex<Real>>& ground_state, unsigned int ell) {
  unsigned int dimension = 1 << ell;

  // Magnetization on Z axis
  complex<Real> Zmagn_gs = 0.;
  complex<Real> mod_Zmagn_gs = 0.;
  for (unsigned i = 0; i < dimension; i++) {
    mod_Zmagn_gs += abs(computational_base_Zmagnetization(i, ell)) * conj(ground_state[i])*ground_state[i];
    Zmagn_gs += computational_base_Zmagnetization(i, ell) * conj(ground_state[i])*ground_state[i];
  }

  // Magnetization on X axis
  complex<Real> Xmagn_gs = 0.;
  for (unsigned i = 0; i < dimension; i++) {
    for (unsigned j = 0; j < ell; j++) {
      Xmagn_gs += conj(ground_state[bit_flip(i, j)]) * ground_state[i];
    }
  }

  // Correlation of consecutives ZZ
  complex<Real> ZZcons_corr = 0.;
  for (unsigned i = 0; i < dimension; i++) {
    ZZcons_corr += conj(ground_state[i])*ground_state[i] * computational_base_ZZcorrelation(i, ell/2-1, ell/2);
  }

  clog << fixed << setprecision(8);
  clog << "Ground state energy: " << ground_energy << ",  deg. " << ground_energy_degeneration << endl;
  clog << "Ground state mean Z-magnetization: " << real(Zmagn_gs)/Real(ell) << endl;
  clog << "Ground state mean modulus Z-magnetization: " << real(mod_Zmagn_gs)/Real(ell) << endl;
  clog << "Ground state mean X-magnetization: " << real(Xmagn_gs)/Real(ell) << endl;
  clog << "Ground state ZZ-magnetization_("<<ell/2-1<<","<<ell/2<<"): " << real(ZZcons_corr) << endl;
  clog << "Energy gap: " << first_excited_energy - ground_energy<< endl;
}

int main(int argc, char *argv[]) {
  string configuration_file(argv[1]);
  string output_folder(argv[2]);

  ifstream configuration_input(configuration_file);
  unsigned int ell, pbc;
  Real h_field, lambda_field;
  configuration_input >> ell >> h_field >> lambda_field >> pbc;

  clog << "Loading configuration..." << endl;
  clog << "  ell = " << ell << endl;
  clog << "  h = " << h_field;
  clog << "  lambda = " << lambda_field << endl;
  clog << "  pbc = " << pbc << endl;
  configuration_input.close();
  

  clog << "Naive diagonalization" << endl;
  auto nd = naive_diagonalization(ell, h_field, lambda_field, pbc);
  analisys(get<0>(nd), get<1>(nd), get<2>(nd), get<3>(nd), ell);

  clog << endl;

  clog << "Lambda Lanczos" << endl;
  auto la = lanczos(ell, h_field, lambda_field, pbc);
  analisys(get<0>(la), get<1>(la), get<2>(la), get<3>(la), ell);

  clog << endl;

  clog << "Spectra Lanczos" << endl;
  auto las = lanczos_spectra(ell, h_field, lambda_field, pbc);
  analisys(get<0>(las), get<1>(las), get<2>(las), get<3>(las), ell);
}