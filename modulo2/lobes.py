
import numpy as np
import matplotlib.pyplot as plt
import sys

from simulations_parameters import ts, Ls, Vs

plt.style.use('seaborn')
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble='\\usepackage{amsmath} \\usepackage{siunitx}')



def main():
  directory = sys.argv[1]
  for L in Ls:
    for V in Vs:
      ground_ = []
      particle_ = []
      hole_ = []
      for t in ts:
        g, h, p = np.loadtxt(directory+f'/{L}-{t}-{V}/result.txt', unpack=True)
        ground_.append(g)
        particle_.append(p)
        hole_.append(h)
      ground = np.array(ground_)
      particle = np.array(particle_)
      hole = np.array(hole_)

      mu_p = particle - ground
      mu_h = ground - hole

      fig, ax = plt.subplots(figsize=(7,6))
      ax.plot(ts, mu_p, label = '$\\mu_p$')
      ax.plot(ts, mu_h, label = '$\\mu_h$')
      ax.set_xlabel('$t$')
      ax.set_ylabel('$\\mu$')
      ax.legend()
      fig.savefig(f'{directory}/plots/lobes-{L}-{V}.pdf', format = 'pdf', bbox_inches = 'tight')
      




if __name__ == '__main__':
  main()