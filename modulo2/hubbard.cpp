#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <random>
#include <string>
#include <utility>
#include <complex>
#include <tuple>
#include <unordered_map>

using namespace std;

#include <Eigen/Dense>

using namespace Eigen;

#include <Spectra/MatOp/SparseSymMatProd.h>
#include <Spectra/SymEigsSolver.h>
#include <Spectra/Util/SelectionRule.h>

using namespace Spectra;

#include <lambda_lanczos.hpp>

using lambda_lanczos::LambdaLanczos;

using Real = double;
using Operator = Matrix<Real, Dynamic, Dynamic>;
using SparseOperator = SparseMatrix<Real>;
using State = Matrix<Real, Dynamic, 1>;

vector<vector<unsigned>> sequential_to_state;

struct StateHasher {
    size_t operator()(const vector<unsigned> &V) const {
        size_t hash = V.size();
        for(auto &i : V) {
            hash ^= i + 0x9e3779b9 + (hash << 6) + (hash >> 2);
        }
        return hash;
    }
};
unordered_map <vector<unsigned>,unsigned,StateHasher> state_to_sequential;

void build_state_table(unsigned actual_particles, unsigned n_sites, unsigned n_particles, unsigned max_site_particles, vector<unsigned>& actual) {
  if (actual.size() == n_sites-1) {
    if (n_particles-actual_particles <= max_site_particles) {
      actual.push_back(n_particles-actual_particles);
      state_to_sequential[actual] = unsigned(sequential_to_state.size());
      sequential_to_state.push_back(actual);
      actual.pop_back();
    }
    return;
  } else {
    for (unsigned i = 0; i <= n_particles-actual_particles and i <= max_site_particles; i++) {
      actual.push_back(i);
      build_state_table(actual_particles + i, n_sites, n_particles, max_site_particles, actual);
      actual.pop_back();
    }
  }
  return;
}

SparseOperator build_hamiltonian(unsigned ell, Real t, Real V, unsigned pbc, unsigned n_particles, unsigned max_site_particles) {
  unsigned int dimension = sequential_to_state.size();
  SparseOperator hamiltonian = SparseOperator(dimension, dimension);

  for (unsigned int i = 0; i < dimension; i++) {
    auto ket = sequential_to_state[i];
    // Hopping
    for (unsigned int j = 0; j < ell-1; j++) {
      //b^dag_j b_(j+1)
      if (ket[j+1] > 0 and ket[j] < max_site_particles) {
        auto bra = ket;
        bra[j]++;
        bra[j+1]--;
        hamiltonian.insert(state_to_sequential[bra], i) -= t * sqrt(Real(ket[j]+1)) * sqrt(Real(ket[j+1]));
      }
      //b^dag_(j+1) b_j
      if (ket[j] > 0 and ket[j+1] < max_site_particles) {
        auto bra = ket;
        bra[j]--;
        bra[j+1]++;
        hamiltonian.insert(state_to_sequential[bra], i) -= t * sqrt(Real(ket[j])) * sqrt(Real(ket[j+1]+1));
      }
    }
    if (pbc == 1) {
      if (ket[0] > 0 and ket[ell-1] < max_site_particles) {
        auto bra = ket;
        bra[ell-1]++;
        bra[0]--;
        hamiltonian.insert(state_to_sequential[bra], i) -= t * sqrt(ket[ell-1]+1) * sqrt(ket[0]);
      } 
      if (ket[ell-1] > 0 and ket[0] < max_site_particles) {
        auto bra = ket;
        bra[ell-1]--;
        bra[0]++;
        hamiltonian.insert(state_to_sequential[bra], i) -= t * sqrt(ket[ell-1]) * sqrt(ket[0]+1);
      }
    }
    //Repulsion
    unsigned rep = 0;
    for (unsigned int j = 0; j < ell; j++) {
      rep += ket[j]*(ket[j]-1)/2;
    }
    hamiltonian.insert(i, i) += Real(rep);
    //Interaction
    Real interaction = 0.;
    for (unsigned int j = 0; j < ell-1; j++) {
      interaction += Real(ket[j]*ket[j+1]);
    }
    if (pbc==1) {
      interaction += Real(ket[ell-1]*ket[0]);
    } else {
      interaction += Real(n_particles)/Real(ell) * (Real(ket[0])+Real(ket[ell-1]));
    }
    hamiltonian.insert(i, i) += V * interaction;
  }

  return hamiltonian;
}

template<typename Value>
Real vector_norm(vector<Value>& v) {
  Real sqn = 0.;
  for (auto& v_: v) {
    sqn += abs(v_)*abs(v_);
  }
  return sqrt(sqn);
}

vector<Real> one_site_occupation_state(unsigned ell, unsigned Nfixed) {
  vector<Real> s(ell, Real(0.));

  for (unsigned i = 0; i < Nfixed; i++) {
    s[i%ell] += Real(1.);
  }
  Real norm = vector_norm(s);
  for (auto& i: s) {
    i /= norm;
  }

  return s;
}

tuple<Real> naive_diagonalization(const Operator& hamiltonian) {
  Eigen::EigenSolver<Operator> eigen_solver(hamiltonian);
  vector<Real> eigenvalues;
  for (auto& ev: eigen_solver.eigenvalues()) {
    eigenvalues.push_back(real(ev));
  }

  return make_tuple(*min_element(eigenvalues.begin(), eigenvalues.end()));
}

tuple<Real> lanczos_(unsigned int ell, unsigned int n_particles, Real t, Real V, Real pbc) {
  unsigned int dimension = sequential_to_state.size();

  auto H_on_ket = [ell, n_particles, dimension, t, V, pbc](const vector<Real>& in, vector<Real>& out) {
    for (unsigned int i = 0; i < dimension; i++) {
      auto ket = sequential_to_state[i];
      // Hopping
      for (unsigned int j = 0; j < ell-1; j++) {
        if (ket[j+1] > 0) {
          auto bra = ket;
          bra[j]++;
          bra[j+1]--;
          out[state_to_sequential[bra]] -= t * in[i] * sqrt(Real(ket[j]+1)) * sqrt(Real(ket[j+1]));
        } 
        if (ket[j] > 0) {
          auto bra = ket;
          bra[j]--;
          bra[j+1]++;
          out[state_to_sequential[bra]] -= t * in[i] * sqrt(Real(ket[j])) * sqrt(Real(ket[j+1]+1));
        }
      }
      if (pbc == 1) {
        if (ket[0] > 0) {
          auto bra = ket;
          bra[ell-1]++;
          bra[0]--;
          out[state_to_sequential[bra]] -= t *in[i] * sqrt(ket[ell-1]+1) * sqrt(ket[0]);
        } 
        if (ket[ell-1] > 0) {
          auto bra = ket;
          bra[ell-1]--;
          bra[0]++;
          out[state_to_sequential[bra]] -= t *in[i] * sqrt(ket[ell-1]) * sqrt(ket[0]+1);
        }
      }
      //Repulsion
      unsigned rep = 0;
      for (unsigned int j = 0; j < ell; j++) {
        rep += ket[j]*(ket[j]-1)/2;
      }
      out[i] += Real(rep) * in[i];
      //Interaction
      Real interaction = 0;
      for (unsigned int j = 0; j < ell-1; j++) {
        interaction += Real(ket[j]*ket[j+1]);
      }
      if (pbc==1) {
        interaction += Real(ket[ell-1]*ket[0]);
      } else {
        interaction += Real(n_particles)/Real(ell) * (Real(ket[0])+Real(ket[ell-1]));
      }
      out[i] += V * interaction *in[i];
    }
  };


  LambdaLanczos<Real> engine(H_on_ket, dimension, false); // false means to calculate the lowest eigenvalue.
  engine.eigenvalue_offset = -50.;
  auto [c_ground_state_energy, ground_state, itern] = engine.run();
  for (auto& v: ground_state) {
    v /= vector_norm(ground_state); // renormalizing eigenstate
  }

  return make_tuple(real(c_ground_state_energy));
}

tuple<Real> lanczos(const SparseOperator& hamiltonian, const double* gs_guess){
  if (hamiltonian.rows() == 1) {
    return make_tuple(hamiltonian.coeff(0,0));
  }

  clog << "\tSparsity rate = " << Real(hamiltonian.nonZeros())/Real(hamiltonian.rows()*hamiltonian.cols()) << endl;

  SparseSymMatProd<Real> hamiltonian_operator(hamiltonian);
  SymEigsSolver<SparseSymMatProd<Real>> engine(hamiltonian_operator, 1, min(100, int(hamiltonian.rows())));
  engine.init(gs_guess);
  int nconv = engine.compute(SortRule::SmallestAlge, 10000);
  if(engine.info() != CompInfo::Successful) {
    cerr << "Unable to compute eigenvalues of reduced H_superblock" << endl;
    exit(1);
  }

  return make_tuple(engine.eigenvalues()[0]);
}


void analisys(Real ground_state_energy, string output_file) {
  clog << fixed << setprecision(16);
  clog << ground_state_energy << endl;

  ofstream res(output_file);
  res << fixed << setprecision(16) << ground_state_energy << endl;
  res.close();
}

void mu_analisys(Real ground_energy, Real particle_energy, Real hole_energy, string output_file) {
  clog << fixed << setprecision(16);
  clog << "Ground state energy: " << ground_energy << endl;
  clog << "Hole energy: " << hole_energy << endl;
  clog << "Particle energy: " << particle_energy << endl;

  clog << "Chem. Pot. Particle: " << particle_energy - ground_energy << endl;
  clog << "Chem. Pot. Hole: " << ground_energy - hole_energy << endl;

  ofstream res(output_file);
  res << fixed << setprecision(16) << ground_energy << '\t' << hole_energy << '\t' << particle_energy << endl;
  res.close();
}

int main(int argc, char *argv[]) {
  string configuration_file(argv[1]);
  string output_file(argv[2]);

  ifstream configuration_input(configuration_file);
  unsigned int ell, n_particles, pbc, algo, max_site_particles;
  Real t, V;
  configuration_input >> ell >> n_particles >> t >> V >> max_site_particles >> pbc >> algo;

  clog << "Loading configuration..." << endl;
  clog << "  ell = " << ell << endl;
  clog << "  n_particles = " << n_particles << endl;
  clog << "  t = " << t << endl;
  clog << "  V = " << V << endl;
  clog << "  max_site_particles = " << max_site_particles << endl;
  clog << "  pbc = " << pbc << endl;
  if (algo == 0) {
    clog << "Using: Naive diagonalization" << endl;
  } else {
    clog << "Using: Lanczos" << endl;
  }
  configuration_input.close();

  if (max_site_particles == 0) {
    max_site_particles = n_particles;
  }
  
  vector<unsigned> tmp;
  if (n_particles != 0) {
    build_state_table(0, ell, n_particles, max_site_particles, tmp);
    SparseOperator hamiltonian = build_hamiltonian(ell, t, V, pbc, n_particles,  max_site_particles);

    assert(state_to_sequential.size()==state_to_sequential.size());
    clog << "Number of states = " << sequential_to_state.size() << endl;
  
    if (algo == 0) {
      auto [ground_state_energy] = naive_diagonalization(Operator(hamiltonian));
      analisys(ground_state_energy, output_file);
    } else if (algo == 1) {
      auto [ground_state_energy] = lanczos(hamiltonian, &one_site_occupation_state(ell, n_particles)[0]);
      analisys(ground_state_energy, output_file);
    }
  } else {

    clog << "Diagonalizig: n_particles = " << ell-1 << endl;
    build_state_table(0, ell, ell-1, max_site_particles, tmp);
    SparseOperator hamiltonian = build_hamiltonian(ell, t, V, pbc, ell-1, max_site_particles);
    Real hole_energy;
    if (algo == 0) {
      auto [e] = naive_diagonalization(Operator(hamiltonian));
      hole_energy = e;
    } else if (algo == 1) {
      auto [e] = lanczos(hamiltonian, &one_site_occupation_state(ell, ell-1)[0]);
      hole_energy = e;
    }

    clog << "Diagonalizig: n_particles = " << ell << endl;
    sequential_to_state.clear();
    state_to_sequential.clear();
    build_state_table(0, ell, ell, max_site_particles, tmp);
    hamiltonian = build_hamiltonian(ell, t, V, pbc, ell, max_site_particles);
    Real ground_energy;
    if (algo == 0) {
      auto [e] = naive_diagonalization(Operator(hamiltonian));
      ground_energy = e;
    } else if (algo == 1) {
      auto [e] = lanczos(hamiltonian, &one_site_occupation_state(ell, ell)[0]);
      ground_energy = e;
    }

    clog << "Diagonalizig: n_particles = " << ell + 1 << endl;
    sequential_to_state.clear();
    state_to_sequential.clear();
    build_state_table(0, ell, ell+1, max_site_particles, tmp);
    hamiltonian = build_hamiltonian(ell, t, V, pbc, ell+1, max_site_particles);
    Real particle_energy;
    if (algo == 0) {
      auto [e] = naive_diagonalization(Operator(hamiltonian));
      particle_energy = e;
    } else if (algo == 1) {
      auto [e] = lanczos(hamiltonian, &one_site_occupation_state(ell, ell+1)[0]);
      particle_energy = e;
    }
    
    mu_analisys(ground_energy, particle_energy, hole_energy, output_file);

  }
}