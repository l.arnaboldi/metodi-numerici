
from pathlib import Path
import sys
import matplotlib.pyplot as plt 
import numpy as np
import subprocess
from tqdm import tqdm
from joblib import Parallel, delayed
import multiprocessing


def full_ground_state(directory, ell, Nfixed, t, V, max_particle_per_site, pbc, algo, hubbard_command):
  with open(directory+f'/tmp/conf-{ell}{Nfixed}{t}{V}{max_particle_per_site}{pbc}{algo}.txt', 'w') as filehandle:
    filehandle.write(f'{ell}\n{Nfixed}\n{t}\n{V}\n{max_particle_per_site}\n{pbc}\n{algo}\n')
  subprocess.run([hubbard_command, directory+f'/tmp/conf-{ell}{Nfixed}{t}{V}{max_particle_per_site}{pbc}{algo}.txt', directory+f'/tmp/result-{ell}{Nfixed}{t}{V}{max_particle_per_site}{pbc}{algo}.txt'])
  energy = np.loadtxt(directory+f'/tmp/result-{ell}{Nfixed}{t}{V}{max_particle_per_site}{pbc}{algo}.txt', unpack = True)
  return energy

def lanczos_vs_exact(directory, hubbard_command, output_directory):
  T = 0.3
  V = 0.
  MAX_OCCUPAION = 4
  PBC = 0

  Ls = np.array(range(1, 10))
  gs_exact = np.array([full_ground_state(directory, L, L, T, V, MAX_OCCUPAION, PBC, 0, hubbard_command) for L in Ls])
  gs_lanczos = np.array([full_ground_state(directory, L, L, T, V, MAX_OCCUPAION, PBC, 1, hubbard_command) for L in Ls])

  fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(20/3*2/1.5,7/1.5))
  # ax.plot(Ls, gs_exact)
  ax.plot(Ls, -gs_lanczos+gs_exact)
  ax.set_xlabel('$\\ell$')
  ax.set_ylabel('$E_0^{(\\text{exact})} - E_0^{(\\text{lanczos})}$')
  fig.savefig(f'{output_directory}/lanczosvsexact.pdf', format = 'pdf', bbox_inches = 'tight')


def max_site_occupation(directory, hubbard_command, output_directory):
  L = 10
  N = L
  T = 0.3
  V = 0.
  PBC = 0
  ALGO = 1

  occupations = np.array(range(2,L+1))
  gs = np.array([full_ground_state(directory, L, N, T, V, occ, PBC, ALGO, hubbard_command) for occ in occupations])

  # With n_mx=1
  fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(20/3*2/1.5,7/1.5))
  ax.plot(occupations[:-1], abs((gs[:-1]-gs[-1])/gs[-1]))#, marker='o', lw=0.8, ls='--', markersize=3)
  ax.set_xlabel('$n_\\text{max}$')
  ax.set_ylabel('$\\frac{E^{(lanczos)}_0 - E_0}{E_0}$')
  ax.set_yscale('log')
  fig.savefig(f'{output_directory}/maxoccupationssite.pdf', format = 'pdf', bbox_inches = 'tight')

  # Without n_mx=1
  fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(20/3/1.5,7/1.5))
  ax.plot(occupations[1:-1], abs((gs[1:-1]-gs[-1])/gs[-1]))#, marker='o', lw=0.8, ls='--', markersize=3)
  ax.set_xlabel('$n_\\text{max}$')
  ax.set_ylabel('$\\frac{E^{(lanczos)}_0 - E_0}{E_0}$')
  ax.set_yscale('log')
  fig.savefig(f'{output_directory}/maxoccupationssitezoom.pdf', format = 'pdf', bbox_inches = 'tight')


def main():
  directory = sys.argv[1] + '/calibration'
  output_directory = sys.argv[2]
  hubbard_command = sys.argv[3]

  Path(directory).mkdir(parents=True, exist_ok=True)
  Path(directory+'/tmp').mkdir(parents=True, exist_ok=True)

  plt.style.use('seaborn')
  plt.rc('text', usetex=True)
  plt.rc('text.latex', preamble='\\usepackage{amsmath} \\usepackage{siunitx}')

  max_site_occupation(directory, hubbard_command, output_directory)
  #lanczos_vs_exact(directory, hubbard_command, output_directory)



if __name__ == '__main__':
  
  main()
