from modulo5.simulation_runner import MAX_SITE_OCCUPATION
from pathlib import Path
import sys
import subprocess
from multiprocessing.pool import ThreadPool

from simulations_parameters import ts, Ls, Vs

N_PROCESS = 4
DIAGONALIZATION_TYPE = 1 # 1 means Lanczos
PARTICLES_NUMBER = 0 # 0 means to compute the chem. pot.s
BOUNDARY_CONDITIONS = 0 # 0 means Open
MAX_SITE_OCCUPATION = 0

def run_simulation(hubbard_command, directory, L, t, V):
  print(f'Computing... L={L}, t={t}, V={V}')
  subprocess.run([hubbard_command, directory+f'/{L}-{t}-{V}/configuration-file.txt', directory+f'/{L}-{t}-{V}/'],
                 stdout=subprocess.DEVNULL,
                 stderr=subprocess.DEVNULL)
  print(f'Done L={L}, t={t}, V={V}')

def main():
  directory = sys.argv[1]
  hubbard_command = sys.argv[2]

  tp = ThreadPool(N_PROCESS)

  for L in Ls:
    for t in ts:
      for V in Vs:
        Path(directory+f'/{L}-{t}-{V}').mkdir(parents=True, exist_ok=True)
        with open(directory+f'/{L}-{t}-{V}/configuration-file.txt', 'w') as filehandle:
          filehandle.write(f'{L}\n{PARTICLES_NUMBER}\n{t}\n{V}\n{MAX_SITE_OCCUPATION}\n{BOUNDARY_CONDITIONS}\n{DIAGONALIZATION_TYPE}\n')
        tp.apply_async(run_simulation, (hubbard_command, directory, L, t, V))
  tp.close()
  tp.join()
  

if __name__ == "__main__":
  main()