
from pathlib import Path
import sys
import matplotlib.pyplot as plt 
import numpy as np
import subprocess
from tqdm import tqdm
from joblib import Parallel, delayed
import multiprocessing


def full_ground_state(directory, ell, Nfixed, t, V, max_particle_per_site, pbc, algo, lanczos_command):
  with open(directory+f'/tmp/conf-{ell}{Nfixed}{t}{V}{max_particle_per_site}{pbc}{algo}.txt', 'w') as filehandle:
    filehandle.write(f'{ell}\n{Nfixed}\n{t}\n{V}\n{max_particle_per_site}\n{pbc}\n{algo}\n')
  subprocess.run([lanczos_command, directory+f'/tmp/conf-{ell}{Nfixed}{t}{V}{max_particle_per_site}{pbc}{algo}.txt', directory+f'/tmp/result-{ell}{Nfixed}{t}{V}{max_particle_per_site}{pbc}{algo}.txt'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
  energy = np.loadtxt(directory+f'/tmp/result-{ell}{Nfixed}{t}{V}{max_particle_per_site}{pbc}{algo}.txt', unpack = True)
  return energy

def DMRG_ground_state(directory, ell, Nfixed, t, V, max_particle_per_site, sweep, DMRG_command):
  with open(directory+f'/tmp/conf-{ell}{Nfixed}{t}{V}{max_particle_per_site}{sweep}.txt', 'w') as filehandle:
    filehandle.write(f'{ell}\n{t}\n{V}\n{Nfixed}\n0\n{max_particle_per_site}\n{sweep}\n')
  subprocess.run([DMRG_command, directory+f'/tmp/conf-{ell}{Nfixed}{t}{V}{max_particle_per_site}{sweep}.txt', directory+f'/tmp/result-{ell}{Nfixed}{t}{V}{max_particle_per_site}{sweep}.txt'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
  energy = np.loadtxt(directory+f'/tmp/result-{ell}{Nfixed}{t}{V}{max_particle_per_site}{sweep}.txt', unpack = True)
  return energy

def DMRG_max_error(directory, ell, Nfixed, t, V, max_particle_per_site, trunc, sweep, DMRG_command):
  with open(directory+f'/tmp/conf-{ell}{Nfixed}{t}{V}{max_particle_per_site}{sweep}.txt', 'w') as filehandle:
    filehandle.write(f'{ell}\n{t}\n{V}\n{Nfixed}\n{trunc}\n{max_particle_per_site}\n{sweep}\n')
  subprocess.run([DMRG_command, directory+f'/tmp/conf-{ell}{Nfixed}{t}{V}{max_particle_per_site}{sweep}.txt', directory+f'/tmp/result-{ell}{Nfixed}{t}{V}{max_particle_per_site}{sweep}.txt'])#, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
  error = np.loadtxt(directory+f'/tmp/result-{ell}{Nfixed}{t}{V}{max_particle_per_site}{sweep}.txt.error.txt', unpack = True)
  return error

def lanczos_vs_DMRG(directory, lanczos_command, DMRG_command, output_directory):
  T = 0.3
  V = 0.
  MAX_OCCUPATION = 4
  PBC = 0
  SWEEP = 2

  Ls = np.array(range(2, 11))

  # Nfixed = L
  gs_DMRG = np.array([DMRG_ground_state(directory, L, L, T, V, MAX_OCCUPATION, SWEEP, DMRG_command) for L in tqdm(Ls)])
  gs_lanczos = np.array([full_ground_state(directory, L, L, T, V, MAX_OCCUPATION, PBC, 1, lanczos_command) for L in tqdm(Ls)])

  fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(20/3*2/1.5,7/1.5))
  ax.plot(Ls, abs((-gs_lanczos+gs_DMRG)/gs_lanczos))
  ax.set_xlabel('$\\ell$')
  ax.set_yscale('log')
  ax.set_ylabel('$E_\\text{ground}^{(\\text{DMRG})} \\text{ absolute error over } E_\\text{ground}^{(\\text{lanczos})}$')
  fig.savefig(f'{output_directory}/lanczosvsdmrgground.pdf', format = 'pdf', bbox_inches = 'tight')

  # Nfixed = L+1
  gs_DMRG = np.array([DMRG_ground_state(directory, L, L+1, T, V, MAX_OCCUPATION, SWEEP, DMRG_command) for L in tqdm(Ls)])
  gs_lanczos = np.array([full_ground_state(directory, L, L+1, T, V, MAX_OCCUPATION, PBC, 1, lanczos_command) for L in tqdm(Ls)])

  fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(20/3*2/1.5,7/1.5))
  ax.plot(Ls, abs((-gs_lanczos+gs_DMRG)/gs_lanczos))
  ax.set_yscale('log')
  ax.set_ylabel('$E_\\text{particle}^{(\\text{DMRG})} \\text{ absolute error over } E_\\text{particle}^{(\\text{lanczos})}$')
  fig.savefig(f'{output_directory}/lanczosvsdmrgparticle.pdf', format = 'pdf', bbox_inches = 'tight')

  # Nfixed = L-1
  gs_DMRG = np.array([DMRG_ground_state(directory, L, L-1, T, V, MAX_OCCUPATION, SWEEP, DMRG_command) for L in tqdm(Ls)])
  gs_lanczos = np.array([full_ground_state(directory, L, L-1, T, V, MAX_OCCUPATION, PBC, 1, lanczos_command) for L in tqdm(Ls)])

  fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(20/3*2/1.5,7/1.5))
  ax.plot(Ls, abs((-gs_lanczos+gs_DMRG)/gs_lanczos))
  ax.set_xlabel('$\\ell$')
  ax.set_yscale('log')
  ax.set_ylabel('$E_\\text{hole}^{(\\text{DMRG})} \\text{ absolute error over } E_\\text{hole}^{(\\text{lanczos})}$')
  fig.savefig(f'{output_directory}/lanczosvsdmrghole.pdf', format = 'pdf', bbox_inches = 'tight')

def sweeps(directory, DMRG_command, output_directory):
  L = 10
  T = 0.3
  V = 0.
  MAX_OCCUPATION = 4

  print("Computing nearly exact energy...")
  exen  = DMRG_ground_state(directory, L, L, T, V, MAX_OCCUPATION, 10, DMRG_command)
  
  Sws = np.array(range(0, 7))
  gs = np.array([DMRG_ground_state(directory, L, L, T, V, MAX_OCCUPATION, s, DMRG_command) for s in tqdm(Sws)])

  fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(20/3*2/1.5,7/1.5))
  ax.plot(Sws, np.log10(abs(gs-exen)/abs(exen)))
  ax.set_xlabel('sweeps')
  ax.set_ylabel('$\\log{\\left|\\frac{E^{(\\text{sweeps})}_0-E^{(10)}_0}{E^{(10)}_0}\\right|}$')
  fig.savefig(f'{output_directory}/sweeps.pdf', format = 'pdf', bbox_inches = 'tight')

def truncation_error(directory, DMRG_command, output_directory):
  T = 0.3
  V = 0.
  MAX_OCCUPATION = 4
  SWEEPS = 1
  TRUNC = 20

  Ls = np.array([5, 10, 20, 40, 80, 100, 160, 250])
  me = np.array([DMRG_max_error(directory, L, L, T, V, MAX_OCCUPATION, TRUNC, SWEEPS, DMRG_command) for L in tqdm(Ls)])

  fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(20/3*2/1.5,7/1.5))
  ax.plot(Ls, me)
  ax.set_xlabel('$\\ell$')
  #ax.set_yscale('log')
  ax.set_ylabel('maximum truncation error')
  fig.savefig(f'{output_directory}/truncationerrortwentylinear.pdf', format = 'pdf', bbox_inches = 'tight')

def time_of_execution(timing_file, output_directory):
  L, ex, la, dmrg, states = np.loadtxt(timing_file, unpack=True)

  # Timing plot
  fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(20/3*2/1.5,7/1.5))
  ax.set_xlabel('$\\ell$')
  ax.plot(L, la, label='Lanczos')
  ax.plot(L, ex, label='Exact')
  ax.plot(L, dmrg, label='DMRG')
  ax.set_yscale('log')
  ax.set_ylabel('time of execution $[\\si{\\second}]$')
  ax.legend()
  fig.savefig(f'{output_directory}/timing.pdf', format = 'pdf', bbox_inches = 'tight')

  # Number of states plot
  fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(20/3*2/1.5,7/1.5))
  ax.set_xlabel('$\\ell$')
  ax.plot(L, states)
  ax.set_yscale('log')
  ax.set_ylabel('number of states')
  fig.savefig(f'{output_directory}/states.pdf', format = 'pdf', bbox_inches = 'tight')


def main():
  directory = sys.argv[1] + '/calibration'
  output_directory = sys.argv[2]
  lanczos_command = sys.argv[3]
  DMRG_command = sys.argv[4]

  Path(directory).mkdir(parents=True, exist_ok=True)
  Path(directory+'/tmp').mkdir(parents=True, exist_ok=True)

  plt.style.use('seaborn')
  plt.rc('text', usetex=True)
  plt.rc('text.latex', preamble='\\usepackage{amsmath} \\usepackage{siunitx}')

  #lanczos_vs_DMRG(directory, lanczos_command, DMRG_command, output_directory)
  sweeps(directory, DMRG_command, output_directory)
  #truncation_error(directory, DMRG_command, output_directory)
  #time_of_execution(sys.argv[5], output_directory)


if __name__ == '__main__':
  
  main()
