#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>

#include <Eigen/Core>
#include <Eigen/SparseCore>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <unsupported/Eigen/KroneckerProduct>

#include <Spectra/MatOp/SparseSymMatProd.h>
#include <Spectra/SymEigsSolver.h>
#include <Spectra/Util/SelectionRule.h>

using namespace std;
using namespace Eigen;
using namespace Spectra;

using Real = double;
using Operator = Matrix<Real, Dynamic, Dynamic>;
using SparseOperator = SparseMatrix<Real>;

SparseOperator H_site, B_site, I_site, N_site;
vector<unsigned> ParticleBase_site;

const Real truncation_error_threshold = Real(1e-8);
const unsigned max_rho_dimension = 30;

Real max_error =  0.;
inline void keep_max_error(Real a) {
  max_error = max<Real>(max_error, a);
}

struct BlockOperators {
  unsigned dimension;
  SparseOperator H, Nleft, Bleft, I;
  vector<unsigned> ParticleBase;

  BlockOperators(
    unsigned dimension_,
    SparseOperator H_,
    SparseOperator Nleft_,
    SparseOperator Bleft_,
    SparseOperator I_,
    vector <unsigned> ParticleBase_
  ) :
    dimension(dimension_), H(H_), Nleft(Nleft_), Bleft(Bleft_), I(I_), ParticleBase(ParticleBase_)
  {}

  BlockOperators(unsigned dimension_) :
    dimension(dimension_)
  {}
};

template <typename BlockIndexType>
tuple<Operator, vector<BlockIndexType>> spectral_truncation_using_blocks(const Operator& rho, const vector<BlockIndexType>& blocks, unsigned truncated_dimension) {
  assert(rho.rows() == rho.cols());
  assert(truncated_dimension <= rho.rows());
  unsigned dimension = rho.rows();

  vector<unsigned> block_indeces(dimension);
  for (unsigned i = 0; i < dimension; i++) {
    block_indeces[i] = i;
  }
  sort(
    block_indeces.begin(),
    block_indeces.end(),
    [blocks](const unsigned& a, const unsigned& b){return blocks[a]<blocks[b];}
  );

  Operator block_base = Operator::Zero(dimension, dimension);
  for (unsigned i = 0; i < dimension; i++) {
    block_base(block_indeces[i], i) = Real(1.);
  }
  Operator rho_block = block_base.transpose() * rho * block_base;


  map<BlockIndexType, unsigned> block_dimensions;
  for (auto be: blocks) {
    block_dimensions[be]++;
  }
  unsigned already_computed_dimension = 0;
  Operator rho_block_eigen_base = Operator::Zero(dimension, dimension);
  vector<pair<Real,pair<BlockIndexType, unsigned>>> eigenvalues;
  for (auto bd: block_dimensions) {
    Operator embed_block = Operator::Zero(bd.second, dimension);
    for (unsigned i = 0; i < bd.second; i++) {
      embed_block(i, already_computed_dimension + i) = Real(1.);
    }
    SelfAdjointEigenSolver<Operator> eigen_engine(rho_block.block(already_computed_dimension, already_computed_dimension, bd.second, bd.second));
    rho_block_eigen_base += embed_block.transpose() * eigen_engine.eigenvectors() * embed_block;

    for (Real ev: eigen_engine.eigenvalues()) {
      eigenvalues.push_back(make_pair(ev, make_pair(blocks[block_indeces[eigenvalues.size()]], block_indeces[eigenvalues.size()])));
    }
    already_computed_dimension += bd.second;
  }
  Operator rho_eigen_base = block_base * rho_block_eigen_base * block_base.transpose();

  sort(
    eigenvalues.begin(),
    eigenvalues.end(),
    [](const auto& a, const auto&b){return a>=b;}
  );
  if (truncated_dimension == 0) {
    Real te = Real(1.);
    for (;truncated_dimension < max_rho_dimension and truncated_dimension < eigenvalues.size() and te >=  truncation_error_threshold; truncated_dimension++) {
      te -= eigenvalues[truncated_dimension].first;
    }
    clog << "\tUsing trun dimension = " << truncated_dimension << endl;
  }
  Operator eigenvector_projector = Operator::Zero(dimension, truncated_dimension);
  vector<BlockIndexType> new_blocks;
  Real total_eigenvalues = Real(0.);
  for (unsigned i = 0; i < truncated_dimension ; i++) {
    eigenvector_projector(eigenvalues[i].second.second, i) = Real(1.);
    new_blocks.push_back(eigenvalues[i].second.first);
    total_eigenvalues += eigenvalues[i].first;
  }
  keep_max_error(Real(1.) - total_eigenvalues);
  cout << "\tTruncation Error: " << Real(1.) - total_eigenvalues << endl;
  
  return {rho_eigen_base * eigenvector_projector, new_blocks};
}

inline void build_site_operators(unsigned max_occupation) {
  B_site = SparseOperator(max_occupation+1, max_occupation+1);
  for (unsigned i = 1; i <= max_occupation; i++) {
    B_site.insert(i-1,i) = sqrt(Real(i));
  }

  N_site = B_site.transpose() * B_site;

  I_site = SparseOperator(max_occupation+1, max_occupation+1);
  for (unsigned i = 0; i <= max_occupation; i++) {
    I_site.insert(i,i) = Real(1.);
  }

  H_site = 0.5 * N_site * (N_site - I_site);

  for (unsigned i = 0; i <= max_occupation; i++) {
    ParticleBase_site.push_back(i);
  }
}

BlockOperators add_site_to_block(const BlockOperators& block, const Real& t, const Real& V, const Real density) {
  BlockOperators bs(block.dimension+1);
  bs.H = kroneckerProduct(block.H, I_site)
        +kroneckerProduct(block.I, H_site)
        -t * kroneckerProduct(block.Bleft.transpose(), B_site)
        -t * kroneckerProduct(block.Bleft, B_site.transpose())
        +V * kroneckerProduct(block.Nleft, N_site);
  // if (block.dimension == 0) {
  //   bs.H += V * density * N_site;
  // }
  bs.Nleft = kroneckerProduct(block.I, N_site);
  bs.Bleft = kroneckerProduct(block.I, B_site);
  bs.I = kroneckerProduct(block.I, I_site);
  bs.ParticleBase.clear();
  for (unsigned pbb: block.ParticleBase) {
    for (unsigned pbs: ParticleBase_site) {
      bs.ParticleBase.push_back(pbb+pbs);
    }
  }
  return bs;
}

tuple<Operator, Real> compute_superblock_ground_state(const BlockOperators& bs_main, const BlockOperators& bs_ancilla, const Real& t, const Real& V, unsigned Nfixed) {
  SparseOperator H_superblock = kroneckerProduct(bs_main.H, bs_ancilla.I) 
                                + kroneckerProduct(bs_main.I, bs_ancilla.H)
                                -t * kroneckerProduct(bs_main.Bleft.transpose(), bs_ancilla.Bleft)
                                -t * kroneckerProduct(bs_main.Bleft, bs_ancilla.Bleft.transpose())
                                +V * kroneckerProduct(bs_main.Nleft, bs_ancilla.Nleft);

  // Find Nfixed block
  vector<unsigned> particles_in_state;
  unsigned state_at_Nfixed = 0;
  for (auto pb_main: bs_main.ParticleBase) {
    for (auto pb_ancilla: bs_ancilla.ParticleBase) {
      if (pb_main + pb_ancilla == Nfixed) {
        state_at_Nfixed++;
        particles_in_state.push_back(0);
      } else {
        particles_in_state.push_back(1);
      }
    }
  }
  cout << "\tNfixed = " << Nfixed << endl;
  vector<unsigned> block_indeces(H_superblock.rows());
  for (unsigned i = 0; i < H_superblock.rows(); i++) {
    block_indeces[i] = i;
  }
  sort(
    block_indeces.begin(),
    block_indeces.end(),
    [particles_in_state](const unsigned& a, const unsigned& b){
      return particles_in_state[a]<particles_in_state[b];
    }
  );
  SparseOperator block_base = SparseOperator(H_superblock.rows(), H_superblock.cols());
  for (unsigned i = 0;  i < particles_in_state.size(); i++) {
    block_base.insert(block_indeces[i], i) = Real(1.);
  }

  SparseOperator Nfixed_hamiltonian = (block_base.transpose() * H_superblock * block_base).block(0,0, state_at_Nfixed, state_at_Nfixed);
  cout << "\tSB dim = " << Nfixed_hamiltonian.rows() << endl;
  clog << "\tSparsity rate of reduced Hsuperblock = " << Real(Nfixed_hamiltonian.nonZeros())/Real(Nfixed_hamiltonian.rows()*Nfixed_hamiltonian.cols()) << endl;
  // Diagonalize block (sparse)
  SparseOperator symmetrized_Nfixed_hamiltonian = 0.5*(SparseOperator(Nfixed_hamiltonian.transpose()) + Nfixed_hamiltonian);
  SparseSymMatProd<Real> super_block_hamiltonian_operator(symmetrized_Nfixed_hamiltonian);
  SymEigsSolver<SparseSymMatProd<Real>> engine(super_block_hamiltonian_operator, 1, int(Nfixed_hamiltonian.rows()));
  engine.init();
  int nconv = engine.compute(SortRule::SmallestAlge, 10000);
  if(engine.info() != CompInfo::Successful) {
    cerr << "Unable to compute eigenvalues of reduced H_superblock" << endl;
    exit(1);
  }

  cout << "\tGround state Energy: " << engine.eigenvalues()[0] << endl;

  return {
    block_base* Operator::Identity(H_superblock.rows(), state_at_Nfixed)*engine.eigenvectors().block(0,0,H_superblock.rows(), 1),
    engine.eigenvalues()[0]
  };
}

inline void compute_and_apply_truncation(const BlockOperators& bs, const Operator& rho, unsigned trunc_dimension, BlockOperators& new_block) {
  // Truncation
  auto [trunc_operator_bs, trunc_ParticleBase_bs] = spectral_truncation_using_blocks(
    rho,
    bs.ParticleBase,
    min<unsigned>(trunc_dimension, unsigned(rho.rows()))
  );

  // Store truncated operators
  assert(new_block.dimension == bs.dimension);
  new_block.H = (trunc_operator_bs.transpose() * bs.H * trunc_operator_bs).sparseView();
  new_block.Nleft = (trunc_operator_bs.transpose() * bs.Nleft * trunc_operator_bs).sparseView();
  new_block.Bleft = (trunc_operator_bs.transpose() * bs.Bleft * trunc_operator_bs).sparseView();
  new_block.I = (trunc_operator_bs.transpose() * bs.I * trunc_operator_bs).sparseView();
  new_block.ParticleBase = trunc_ParticleBase_bs;
}

void DMRG_infinite_step(const BlockOperators& block, unsigned trunc_dimension, BlockOperators& new_block, const Real& t, const Real& V, const unsigned Nfixed, const unsigned L) {
  Real density = Real(Nfixed)/Real(L);

  BlockOperators bs = add_site_to_block(block, t, V, density);
  auto [ground_state, ground_energy] = compute_superblock_ground_state(bs, bs, t, V, Nfixed);

  // Partial Trace
  Matrix<Real, Dynamic, Dynamic> ground_state_matrix = ground_state.reshaped(bs.H.rows(), bs.H.cols());
  Operator rho_bs = ground_state_matrix * ground_state_matrix.transpose();

  compute_and_apply_truncation(bs, rho_bs, trunc_dimension, new_block);
}

void DMRG_finite_step(const BlockOperators& block, const BlockOperators& ancilla, unsigned trunc_dimension, BlockOperators& new_block, const Real& t, const Real& V, const unsigned Nfixed, const unsigned L) {
  Real density =Real(Nfixed)/Real(L);
  
  BlockOperators bs = add_site_to_block(block, t, V, density);
  BlockOperators as = add_site_to_block(ancilla, t, V, density);
  auto [ground_state, ground_energy] = compute_superblock_ground_state(bs, as, t, V, Nfixed);

  // Partial Trace
  Matrix<Real, Dynamic, Dynamic> ground_state_matrix = ground_state.reshaped(as.H.cols(), bs.H.rows());
  Operator rho_bs = ground_state_matrix.transpose() * ground_state_matrix;
  assert(rho_bs.rows() == bs.H.rows());
  
  compute_and_apply_truncation(bs, rho_bs, trunc_dimension, new_block);
}

int main(int argc, char *argv[]) {
  string configuration_file(argv[1]);
  string output_file(argv[2]);

  unsigned int ell, Nfixed, trunc, max_site_occupation, sweeps;
  Real t, V;

  ifstream configuration_input(configuration_file);
  configuration_input >> ell >> t >> V >> Nfixed >> trunc >> max_site_occupation >> sweeps;
  clog << "Loading configuration..." << endl;
  clog << "  ell = " << ell << endl;
  clog << "  t = " << t << endl;
  clog << "  V = " << V << endl;
  clog << "  Nfixed = " << Nfixed << endl;
  clog << "  trunc = " << trunc << endl; // if trunc==0 we keep enough state to have truncation_error < truncation_error_threshold
  clog << "  max_site_occupation = " << max_site_occupation << endl;
  clog << "  sweeps = " << sweeps << endl;
  configuration_input.close();

  build_site_operators(max_site_occupation);

  vector<BlockOperators> block;

  // Operator of the empty chain
  block.push_back(BlockOperators(0));
  block[0].H = SparseOperator(1,1);
  block[0].I = SparseOperator(1,1);
  block[0].I.insert(0,0) = Real(1.);
  block[0].Bleft = SparseOperator(1,1);
  block[0].Nleft = SparseOperator(1,1);
  block[0].ParticleBase.push_back(0);

  // Infinite DMRG
  unsigned l;
  for (l = 1; 2*l <= ell; l++) {
    cout << "Computing block of size " << l << endl;
    block.push_back(BlockOperators(l));
    DMRG_infinite_step(block[l-1], trunc, block[l], t, V, max<unsigned>(unsigned(Real(2*l)/Real(ell)*Real(Nfixed)), 1), 2*l);
  }
  // Finite until max lenght (ell-1)
  for (; l <= ell-1; l++) {
    cout << "Computing (finite) block of size " << l << endl;
    block.push_back(BlockOperators(l));
    DMRG_finite_step(block[l-1], block[ell-l-1], trunc, block[l], t, V, Nfixed, ell);
  }

  // Sweeps
  for (unsigned s = 1; s <= sweeps; s++) {
    for (unsigned l=1; l <= ell-1; l++) {
      cout << "Sweep " << s << " -  block " << l << endl;
      DMRG_finite_step(block[l-1], block[ell-l-1], trunc, block[l], t, V, Nfixed, ell);
    }
  }

  // Measure
  cout << "Computing measure" << endl;
  BlockOperators right_block = add_site_to_block(block[(ell-2)/2], t, V, Real(Nfixed)/Real(ell));
  BlockOperators left_block = add_site_to_block(block[(ell-1)/2], t, V, Real(Nfixed)/Real(ell));
  auto [ground_state, ground_energy] = compute_superblock_ground_state(left_block, right_block, t, V, Nfixed);

  ofstream result_file(output_file);
  result_file << fixed << setprecision(16) << ground_energy << endl;
  result_file.close();

  ofstream error_file(output_file+".error.txt");
  error_file << fixed << setprecision(16) << max_error << endl;
  error_file.close();
}