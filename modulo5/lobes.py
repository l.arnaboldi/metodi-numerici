from LabTools.utils.uncertainties import de2unc
import numpy as np
from uncertainties import unumpy as unp
from uncertainties import ufloat
import sys
from scipy.optimize import curve_fit

from tqdm import tqdm

from LabTools.fit import *
from LabTools.plot import *
import matplotlib.pyplot as plt

from simulations_parameters import ts, Ls, Vs, ts_half

MAX_T_TRANSICTION = 0.35

def parabola(x, m, q, a):
  return m*x+q+a*x*x

def compute_point(t, V, directory, plots = False, output_directory = None):
  Lsr = Ls
  mu_up = []
  mu_down = []
  for L in Lsr:
    gs = np.loadtxt(directory+f'/{L}-{t}-{V}/result-ground.txt', unpack = True)
    ps = np.loadtxt(directory+f'/{L}-{t}-{V}/result-particle.txt', unpack = True)
    hs = np.loadtxt(directory+f'/{L}-{t}-{V}/result-hole.txt', unpack = True)
    mu_up.append(ps-gs)
    mu_down.append(gs-hs)
  mu_up = np.array(mu_up)
  mu_down = np.array(mu_down)

  pp, ppcov = curve_fit(parabola, 1./Lsr, mu_up)
  ph, phcov = curve_fit(parabola, 1./Lsr, mu_down)

  mu_particle = ufloat(pp[1], np.sqrt(ppcov[1][1]))
  mu_hole = ufloat(ph[1], np.sqrt(phcov[1][1]))

  if plots:
    tspace = np.linspace(-0.004, .104, 10)

    fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(20/3*2/1.5,7/1.5))
    ax.plot(1/Lsr, mu_up, ls='', marker='.', c = 'blue')
    ax.plot(tspace, parabola(tspace, *pp), lw = 0.1)
    ax.set_xlabel('$\\frac{1}{L}$')
    ax.set_ylabel('$\\mu^p_L$')
    fig.savefig(f'{output_directory}/singlepointfitparticle.pdf', format = 'pdf', bbox_inches = 'tight')

    fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(20/3*2/1.5,7/1.5))
    ax.plot(1/Lsr, mu_down, ls='', marker='.', c = 'blue')
    ax.plot(tspace, parabola(tspace, *ph), lw = 0.1)
    ax.set_xlabel('$\\frac{1}{L}$')
    ax.set_ylabel('$\\mu^h_L$')
    fig.savefig(f'{output_directory}/singlepointfithole.pdf', format = 'pdf', bbox_inches = 'tight')
  return mu_particle, mu_hole

def main():
  directory = sys.argv[1]
  output_directory = sys.argv[2]

  plt.style.use('seaborn')
  plt.rc('text', usetex=True)
  plt.rc('text.latex', preamble='\\usepackage{amsmath} \\usepackage{siunitx}')

  # t = 0. is not simulated correctly for the hole
  tsr = ts[1:]

  #compute_point(0.1105263157894737, 0.4, directory, True, output_directory)
  compute_point(tsr[18], 0.4, directory, True, output_directory)

  for V in Vs:
    mu_particle, mu_hole = tuple(map(list, zip(*[compute_point(t, V, directory) for t in tqdm(tsr)])))

    mu_particle, u_mu_particle = unpack_unarray(np.array(mu_particle))
    mu_hole, u_mu_hole= unpack_unarray(np.array(mu_hole))

    # Plot the lobe
    fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(20/3*2/1.5,7/1.5))
    ax.plot(tsr, mu_particle, ls='--', marker='', lw = 0.1, c='green')
    ax.errorbar(tsr, mu_particle, u_mu_particle, label = '$\\mu^p$', fmt='o', markersize=2, capsize=1, linestyle='', elinewidth=0.4, c='green')
    ax.plot(tsr, mu_hole, ls='--', marker='', lw = 0.1, c='red')
    ax.errorbar(tsr, mu_hole, u_mu_hole, label = '$\\mu^h$', fmt='o', markersize=2, capsize=1, linestyle='', elinewidth=0.4, c='red')
    ax.set_xlabel('$t$')
    ax.set_ylabel('$\\mu$')
    ax.legend()
    fig.savefig(f'{output_directory}/lobes.pdf', format = 'pdf', bbox_inches = 'tight')

    relative_error = abs((mu_particle-mu_hole)/(mu_particle+mu_hole))
    best_i = 0
    for i in range(len(relative_error)):
      if tsr[i] > MAX_T_TRANSICTION:
        break
      if relative_error[i] < relative_error[best_i]:
        best_i = i
    
    spike = ufloat((tsr[best_i], (-tsr[best_i-1]+tsr[best_i+1])/2.))
    print(f'Spike at {spike}')
    
    # Plot the difference
    fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(20/3*2/1.5,7/1.5))
    ax.plot(tsr, relative_error)
    ax.errorbar(tsr[best_i], relative_error[best_i], marker='o', c='red')
    ax.set_xlabel('$t$')
    ax.set_ylabel('$\\left|\\frac{\\mu^p-\\mu^h}{\\mu^p+\\mu^h}\\right|$')
    ax.set_yscale('log')
    fig.savefig(f'{output_directory}/lobesdifference.pdf', format = 'pdf', bbox_inches = 'tight')

    # Plot the difference (zoommed)
    fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(20/3*2/1.5,7/1.5))
    ax.plot(tsr[50:], relative_error[50:])
    ax.plot(tsr[best_i], relative_error[best_i], marker='x', c='blue')
    ax.set_xlabel('$t$')
    ax.set_ylabel('$\\left|\\frac{\\mu^p-\\mu^h}{\\mu^p+\\mu^h}\\right|$')
    ax.set_yscale('log')
    fig.savefig(f'{output_directory}/lobesdifferencezoomed.pdf', format = 'pdf', bbox_inches = 'tight')






if __name__ == '__main__':
  main()