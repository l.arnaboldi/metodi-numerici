from pathlib import Path
import sys
import subprocess
from multiprocessing.pool import ThreadPool

from simulations_parameters import ts, Ls, Vs, ts_half

N_PROCESS = 22
TRUNCATION_DIMENSION = 20
MAX_SITE_OCCUPATION = 3
SWEEPS = 0

def run_simulation(hubbard_command, directory, L, t, V, different = ''):
  print(f'Computing... L={L}, t={t}, V={V} {different}')
  if different != '':
    different += '/'
  if not Path(directory+f'/{different}{L}-{t}-{V}/result-hole.txt').is_file():
    subprocess.run([hubbard_command, directory+f'/{different}{L}-{t}-{V}/configuration-file-hole.txt', directory+f'/{different}{L}-{t}-{V}/result-hole.txt', different],
                  stdout=subprocess.DEVNULL,
                  stderr=subprocess.DEVNULL)
  else:
    print(f'  Hole L={L}, t={t}, V={V} {different} exists')

  if not Path(directory+f'/{different}{L}-{t}-{V}/result-ground.txt').is_file():
    subprocess.run([hubbard_command, directory+f'/{different}{L}-{t}-{V}/configuration-file-ground.txt', directory+f'/{different}{L}-{t}-{V}/result-ground.txt', different],
                  stdout=subprocess.DEVNULL,
                  stderr=subprocess.DEVNULL)
  else:
    print(f'  Ground L={L}, t={t}, V={V} {different} exists')

  if not Path(directory+f'/{different}{L}-{t}-{V}/result-particle.txt').is_file():
    subprocess.run([hubbard_command, directory+f'/{different}{L}-{t}-{V}/configuration-file-particle.txt', directory+f'/{different}{L}-{t}-{V}/result-particle.txt', different],
                  stdout=subprocess.DEVNULL,
                  stderr=subprocess.DEVNULL)
  else:
    print(f'  Particle L={L}, t={t}, V={V} {different} exists')
  print(f'Done L={L}, t={t}, V={V} {different}')


def main():
  directory = sys.argv[1]
  hubbard_command = sys.argv[2]

  tp = ThreadPool(N_PROCESS)

  for L in Ls:
    for t in ts:
      for V in Vs:
        Path(directory+f'/{L}-{t}-{V}').mkdir(parents=True, exist_ok=True)
        with open(directory+f'/{L}-{t}-{V}/configuration-file-hole.txt', 'w') as filehandle:
          filehandle.write(f'{L}\n{t}\n{V}\n{L-1}\n{TRUNCATION_DIMENSION}\n{MAX_SITE_OCCUPATION}\n{SWEEPS}')
        with open(directory+f'/{L}-{t}-{V}/configuration-file-ground.txt', 'w') as filehandle:
          filehandle.write(f'{L}\n{t}\n{V}\n{L}\n{TRUNCATION_DIMENSION}\n{MAX_SITE_OCCUPATION}\n{SWEEPS}')
        with open(directory+f'/{L}-{t}-{V}/configuration-file-particle.txt', 'w') as filehandle:
          filehandle.write(f'{L}\n{t}\n{V}\n{L+1}\n{TRUNCATION_DIMENSION}\n{MAX_SITE_OCCUPATION}\n{SWEEPS}')
        tp.apply_async(run_simulation, (hubbard_command, directory, L, t, V))
  
  try:
    if sys.argv[3] != 'half':
      raise(KeyError())
    Path(directory+f'/half').mkdir(parents=True, exist_ok=True)
    for L in Ls:
      for t in ts_half:
        for V in Vs:
          Path(directory+f'/half/{L}-{t}-{V}').mkdir(parents=True, exist_ok=True)
          with open(directory+f'/half/{L}-{t}-{V}/configuration-file-hole.txt', 'w') as filehandle:
            filehandle.write(f'{L}\n{t}\n{V}\n{int(L/2-1)}\n{TRUNCATION_DIMENSION}\n{MAX_SITE_OCCUPATION}\n{SWEEPS}')
          with open(directory+f'/half/{L}-{t}-{V}/configuration-file-ground.txt', 'w') as filehandle:
            filehandle.write(f'{L}\n{t}\n{V}\n{int(L/2)}\n{TRUNCATION_DIMENSION}\n{MAX_SITE_OCCUPATION}\n{SWEEPS}')
          with open(directory+f'/half/{L}-{t}-{V}/configuration-file-particle.txt', 'w') as filehandle:
            filehandle.write(f'{L}\n{t}\n{V}\n{int(L/2+1)}\n{TRUNCATION_DIMENSION}\n{MAX_SITE_OCCUPATION}\n{SWEEPS}')
          tp.apply_async(run_simulation, (hubbard_command, directory, L, t, V, 'half'))
  except:
    print('No half!')
  tp.close()
  tp.join()
  

if __name__ == "__main__":
  main()