#include <iostream>
#include <Eigen/Core>
#include <Eigen/SparseCore>
#include <Eigen/Dense>
#include <unsupported/Eigen/KroneckerProduct>
#include <Spectra/MatOp/SparseSymMatProd.h>
#include <Spectra/SymEigsSolver.h>
#include <Spectra/Util/SelectionRule.h>


using namespace std;
using namespace Eigen;
using namespace Spectra;
using Real = double;
using Operator = Matrix<Real, Dynamic, Dynamic>;

template <typename BlockIndexType>
tuple<Operator, vector<BlockIndexType>> spectral_truncation_using_blocks(Operator& rho, vector<BlockIndexType>& blocks, unsigned truncated_dimension) {
  assert(rho.rows() == rho.cols());
  assert(truncated_dimension <= rho.rows());
  unsigned dimension = rho.rows();

  vector<unsigned> block_indeces(dimension);
  for (unsigned i = 0; i < dimension; i++) {
    block_indeces[i] = i;
  }
  sort(
    block_indeces.begin(),
    block_indeces.end(),
    [blocks](const unsigned& a, const unsigned& b){return blocks[a]<blocks[b];}
  );

  Operator block_base = Operator::Zero(dimension, dimension);
  for (unsigned i = 0; i < dimension; i++) {
    block_base(block_indeces[i], i) = Real(1.);
  }
  Operator rho_block = block_base.transpose() * rho * block_base;
  //cout << rho_block << endl;

  map<BlockIndexType, unsigned> block_dimensions;
  for (auto be: blocks) {
    block_dimensions[be]++;
  }
  unsigned already_computed_dimension = 0;
  Operator rho_block_eigen_base = Operator::Zero(dimension, dimension);
  vector<pair<Real,unsigned>> eigenvalues;
  for (auto bd: block_dimensions) {
    //cout << "Block " << bd.first << ", dimension " << bd.second << endl;
    Operator embed_block = Operator::Zero(bd.second, dimension);
    for (unsigned i = 0; i < bd.second; i++) {
      embed_block(i, already_computed_dimension + i) = Real(1.);
    }
    //cout << embed_block << endl << endl;
    SelfAdjointEigenSolver<Operator> eigen_engine(rho_block.block(already_computed_dimension, already_computed_dimension, bd.second, bd.second));
    rho_block_eigen_base += embed_block.transpose() * eigen_engine.eigenvectors() * embed_block;
    //cout << embed_block.transpose() * eigen_engine.eigenvectors() * embed_block << endl;
    for (Real ev: eigen_engine.eigenvalues()) {
      eigenvalues.push_back(make_pair(ev, block_indeces[eigenvalues.size()]));
    }
    already_computed_dimension += bd.second;
  }
  //cout << rho_block_eigen_base << endl;
  Operator rho_eigen_base = block_base * rho_block_eigen_base * block_base.transpose();
  cout << rho_eigen_base << endl;

  sort(
    eigenvalues.begin(),
    eigenvalues.end(),
    [](const auto& a, const auto&b){return a>=b;}
  );

  Operator eigenvector_projector = Operator::Zero(dimension, truncated_dimension);
  vector<BlockIndexType> new_blocks;
  for (unsigned i = 0; i < truncated_dimension; i++) {
    eigenvector_projector(eigenvalues[i].second, i) = Real(1.);
    new_blocks.push_back(blocks[eigenvalues[i].second]);
  }
  //cout << endl << eigenvector_projector << endl;
  
  return {rho_eigen_base * eigenvector_projector, new_blocks};
}

void generic_test() {
  SparseMatrix<double> Hblock(2,2);
  SparseMatrix<double> Hsite(2,2);
  Hblock.insert(0, 0) = 1.;
  Hblock.insert(1, 1) = 3.;
  Hblock.insert(1, 0) = 0.;
  Hblock.insert(0, 1) = 0.;

  Hsite.insert(0, 0) = 7.;
  Hsite.insert(1, 1) = 19.;
  Hsite.insert(1, 0) = 0.;
  Hsite.insert(0, 1) = 0.;

  cout << Hblock << endl;
  cout << Hsite << endl;


  SparseMatrix<double> Hcombined = kroneckerProduct(Operator::Identity(2,2), Hsite) + kroneckerProduct(Hblock, Operator::Identity(2,2));

  cout << Hcombined << endl;

  SparseSymMatProd<double> super_block_operator(Hcombined);
  SymEigsSolver<SparseSymMatProd<double> > engine(super_block_operator, 1, 2);
  engine.init();
  int nconv = engine.compute(SortRule::SmallestAlge);
  auto ground_state = engine.eigenvectors();
  cout << ground_state << endl;
  Matrix<double, Dynamic, Dynamic> rho = kroneckerProduct(ground_state, ground_state.transpose());
  cout << rho << endl;
  // Devo fare un reshape del tipo:
  //
  // a          
  // b  ------> a c
  // c          b d
  // d
  // 
  Matrix<double, Dynamic, Dynamic> ground_state_matrix = ground_state.reshaped(2,2);
  cout << ground_state_matrix << endl;
  auto rhoA = ground_state_matrix * ground_state_matrix.transpose();
  cout << rhoA << endl;

  cout << rho.block(0,0, 2, 2) << endl;
}

int main() {

  vector<unsigned> b = {1, 5, 1, 0};
  
  Operator rho = Operator::Zero(4, 4);
  rho(0,0) = 0.5;
  rho(1,1) = 7.;
  rho(0,2) = -0.5;
  rho(2,0) = -0.5;
  rho(2,2) = 0.5;
  rho(3,3) = 14.5;

  cout << rho << endl;
  spectral_truncation_using_blocks(rho, b, 2);


}